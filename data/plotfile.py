# -*- coding: utf-8 -*-
#Author Martin Soderen
import numpy as np
import matplotlib.pyplot as plt
import argparse
import plotlib

def plotfileFunc(filename,bunchesToPlot,notch=False):
  file=plotlib.PlotFile(filename)
  if not file.open:
    raise RuntimeError("File could not be opened")

  #If we have not been told which bunches to plot we plot all
  if len(bunchesToPlot)==0:
    bunchesToPlot=[x for x in range(3564)]
  #Another sanity check
  for element in file.datasets:
    if max(bunchesToPlot)>element.cols-1:
      raise RuntimeError("Bunch not found in dataset")

  #Setup plts
  f, axarr=plt.subplots(len(file.datasets),sharex=len(file.datasets)>1)

  #Plot all datasets
  for element in file.datasets:
    ax1=None
    maxarr=0
    ylimits=[0,0]
    if len(file.datasets)>1:
      ax1=axarr[index]
    else:
      ax1=axarr
    ax1.grid(True)
    ax1.set_title("Data captured from "+element.group+" "+element.dataset)
    ax1.set_ylabel("Bunch position [A.U.]")
    ax1.set_xlabel("Turn number")
    #For every bunch in the dataset
    for i in range(element.cols):
      tempdata=None
      #Check if it only contains zeros
      if element.data[i][0]==0 and element.data[i][int(len(element.data[i])/2)]==0 and element.data[i][-1]==0:
        continue
      #if we dont want to plot this bunch
      if i not in bunchesToPlot:
        continue
      if notch:
        tempdata=np.convolve(element.data[i],np.array([1,-1]))[1:-1]
      else:
        tempdata=element.data[i]
      ax1.plot(range(len(tempdata)),tempdata,label="bunch "+str(i))
      if ylimits[1]==0:
        ylimits[1]=max(tempdata)
      else:
        ylimits[1]=max(ylimits[1],max(tempdata))
      if ylimits[0]==0:
        ylimits[0]=min(tempdata)
      else:
        ylimits[0]=min(ylimits[0],min(tempdata))
      maxarr=max(maxarr,len(tempdata))
    ax1.set_xlim([0,maxarr])
    ax1.set_ylim(ylimits)
  plt.savefig(file.PathAndFilename)
  plt.close("all")
  file.close()

if __name__ == "__main__":
  parser=argparse.ArgumentParser()
  parser.add_argument("-notch","-n", help="run every bunch through a notch filter before plotting",action="store_true")
  parser.add_argument("-bunches","-b", nargs='*', help='<Required> Set flag', type=int)
  parser.add_argument("-filename","-f",required=True)
  args = parser.parse_args()
  bunchesToPlot=[]
  if args.bunches!=None:
    bunchesToPlot=args.bunches
  plotfileFunc(args.filename,bunchesToPlot,args.notch)