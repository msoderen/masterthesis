import os
import h5py
import numpy as np
import matplotlib.pyplot as plt
from cycler import cycler
#plt.rcParams['axes.prop_cycle'] = cycler(color='bgrcmyk')

plt.rcParams.update({'font.size': 19})
plt.style.use('classic')
f=h5py.File("InjInst_B2_234110_294468-hb2-144.h5",'r')
dataset=None
for x in f.keys():
	for y in f[x].keys():
		dataset=f[x][y]
t,axarr=plt.subplots(1,1)
axarr.plot(range(len(dataset)),dataset)
axarr.grid(True)
axarr.set_title("Data captured from B2 horizontal")
axarr.set_ylabel("Bunch position [A.U.]")
axarr.set_xlabel("Turn number")
axarr.set_xlim([0,len(dataset)])
axarr.set_ylim([min(dataset),max(dataset)])
#plt.savefig("adawda.png")
#plt.close("all")
#f.close()
plt.show()
