# -*- coding: utf-8 -*-
#Author Martin Soderen
import os
import h5py
import numpy as np
#os.system("export PATH=/user/bdisoft/operational/bin/Python/PRO/bin/:$PATH")
rootpath="/home/msoderen/repositories/masterthesis/data"
#For easy structuring
class Dataset:
  def  __init__(self,group,dataset,data):
    self.group=group
    self.dataset=dataset
    self.data=data
    self.rows=data.shape[1]
    self.cols=data.shape[0]
  def __str__(self):
    return self.group+"/"+self.dataset+": rows: "+str(self.rows)+" cols: "+str(self.cols)

class PlotFile:
  def __init__(self,filename):
    self.open=False
    self.filename=filename
    #sanity checks
    if not os.path.isfile(self.filename):
      return
    try:
      self.file=h5py.File(self.filename,'r')
    except OSError:
      return
    #extract fillnumber
    self.fill=self.filename.split("_")[-6].split("/")[-1]
    if self.fill[0]=="0":
      self.fill=self.fill[1:]
    #create real filename
    self.justfilename=self.filename.split("/")[-1].split(".h5")[0]+".png"
    #create path
    self.path=rootpath+"/plots/"+self.fill
    if not os.path.exists(self.path):
      os.makedirs(self.path)
    self.PathAndFilename=self.path+"/"+self.justfilename
    #extract all datasets
    self.datasets=[]
    for group in self.file.keys():
      for dataset in self.file[group].keys():
        data=np.transpose(self.file[group][dataset])
        if data.shape[0]>0 and data.shape[1]>0:
          self.datasets.append(Dataset(group,dataset,data))
    self.open=True

  def close(self):
    self.file.flush()
    self.file.close()
