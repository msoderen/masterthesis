\documentclass[thesis.tex]{subfiles}
\begin{document}

\chapter{Instantaneous Amplitude Calculation}
\label{cha:amplitude}
The data that is received from the ObsBoxBuffer FESA class contains the transverse position for each bunch each turn as 16~bit signed integers. To simplify the detection of a potential instability, it makes sense to calculate the instantaneous amplitude for each bunch. This chapter describes first how a vector of 16~bit signed integers can be converted to a vector of single precision floating points very fast using Intel intrinsics in Sec.~\ref{sec:conversion} and how the instantaneous amplitude can be calculated using the Hilbert transform in Sec.~\ref{sec:hilbert}.

\section{Fast 16~bit Signed Integer to Single Precision Floating Point Conversion Using Intel Intrinsics}
\label{sec:conversion}
The first step to calculate the instantaneous amplitude from the positional data is to convert the data to single precision floating points. There is no instruction for direct 16~bit signed integer to single precision floating-point conversion so there must be an intermediate step to convert it to 32~bit signed integer. This can be done very efficiently using AVX registers and Intel intrinsics in the following manner:
\begin{lstlisting}[language=C++,caption={[Fast 16~bit signed to float conversion]Fast 16~bit signed integer to single precision floating point conversion using Intel Intrinsics}]
//load 8 short int
__m128i a0 = _mm_loadu_si128(data);
//split into two registers
__m128i b0 = _mm_unpackhi_epi64(a0, a0);
//convert to 32 bit integers
a0 = _mm_cvtepi16_epi32(a0);
b0 = _mm_cvtepi16_epi32(b0);
//convert to 32 bit float
__m128 c0 = _mm_cvtepi32_ps(a0);
__m128 d0 = _mm_cvtepi32_ps(b0);
\end{lstlisting}

\section{Transverse Oscillation Amplitude Calculation Using the Hilbert Transform}
\label{sec:hilbert}
After the single precision floating-point conversion the instantaneous oscillation amplitude for each bunch can be calculated. To calculate the instantaneous amplitude, the Hilbert transform can be used \cite{salih:2012}\cite{david:2012}\cite{alan1989discrete}. The Hilbert transform can be implemented as a FIR (\textit{Finite Impulse Response}) filter and can be tuned for the appropriate frequency band. Using the Hilbert transform, the analytic signal for the real signal is calculated from which the instantaneous amplitude can be calculated.
\\
\\
A real signal $x_r[n]$ is a one-dimensional array of real values over time. This signal normally has positive and negative frequency components with a symmetry around the zero-frequency point. This signal can be extended by a companion function $x_i[t]$ so that the resulting signal only has positive frequency components. The relation between $x_r[n]$ and $x_i[t]$ is related through the Hilbert transform \cite{salih:2012}\cite{david:2012}.
\begin{equation} 
\label{eq:hilbert}
 x_c[n]=x_r[n]+jx_i[n]
\end{equation}  
Where $x_c[t]$ also can be expressed as
\begin{equation} 
 x_c[n]=A[n]e^{j\phi[n]}
\end{equation}
where
\begin{equation} 
\label{eq:amplitude}
 A[n]=\sqrt{x_r^2[n]+x_i^2[n]}
\end{equation}
The Hilbert relationship is
\begin{equation} 
\mathcal{H}[x_r[n]]=x_i[n]
\end{equation}
which can also be expressed as
\begin{equation} 
\label{eq:causal}
x_i[n]=\sum_{m=-\infty}^{\infty}h[n-m]x_r[m]
\end{equation}
where:
  \begin{equation}
  \label{eq:hn}
    h[n]=
    \begin{cases}
      \dfrac{2}{\pi}\dfrac{sin^2(\pi n/2)}{n}, & \text{if}\ n\neq0 \\
      0, & n=0
    \end{cases}
  \end{equation}
However, Eq.~\ref{eq:causal} is not really helpful since it is not absolutely summable and also non-causal. That is when a discrete Hilbert transformer of order M can be created with a Kaiser window approximation which, instead of Eq.~\ref{eq:hn}, uses:
  \begin{equation}
  \label{eq:kaiser}
    h[n]
    \begin{cases}
      \dfrac{2}{\pi}\dfrac{sin^2[\pi (n-n_d)/2]}{n-n_d}, & \text{if}\ 0\leq n\leq M \\
      0, & \text{otherwise}
    \end{cases}
  \end{equation}
  where:
  \begin{equation} 
n_d=M/2
\end{equation}
So for example with $M=6$:
  \begin{align}
    h &= \begin{bmatrix}
           -0.2122 &
           0 &
           -0.6366 &
           0 &
           0.6366 &
            0&
           0.2122
         \end{bmatrix}
         \label{eq:hilbertcalc}
  \end{align}
  
\subsection{Optimizing the Hilbert Transformer for the LHC}
\label{sec:hilbertopt}
Since the frequency of the transverse oscillation in the LHC is known \cite{Steinhagen:1213281} there is a potential for optimization. The FIR filter can be tuned so the magnitude response for the tune band can be flat. By using the Matlab Filter Designer \cite{matlabfilter} and creating a Hilbert FIR filter of order 6 tuned to the normalized band $[0.27~ 0.32]$ the following coefficients were acquired:
  \begin{align}
    h &= \begin{bmatrix}
           -0.0906 &
           -0.0198 &
           -0.5941 &
           0 &
           0.5941&
            0.0198&
           0.0906
         \end{bmatrix}
         \label{eq:hilbertmatlab}
  \end{align}
The two filters were compared by feeding both with a sinusoidal wave with a normalized frequency of 0.305 and with fixed amplitude of 10000 to simulate the transverse oscillation. For each signal the amplitude was calculated and the ripple was compared as can be seen in Fig.~\ref{fig:hilbertfilter}. The frequency response of the two filter can be seen in Fig.~\ref{fig:filtergain} where it is clear that the optimized filter has a much flatter magnitude response in the relevant band.
\begin{figure}[H]
\centering
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/vectorreconstuction.png}
  \captionof{figure}{Comparison of amplitude ripple between two Hilbert filters}
  \label{fig:hilbertfilter}
\end{minipage}%
\hspace{2mm}
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{figures/filtergain.png}
  \captionof{figure}[Frequency response of two different filters]{Frequency response of the two different filters. The optimized filter has a flat frequency response in the relevant band while the same band in the generic filter differs several dB}
  \label{fig:filtergain}
\end{minipage}
\end{figure}


\begin{figure}[H]
\center
\includegraphics[width=0.7\textwidth]{figures/vectorReconNew.png}
\caption{Comparison of the vector reconstructions using the two different filters}
\label{fig:vectorReconstructionNew}
\end{figure}
\noindent
The optimized filter generated from Matlab requires 7 multiplications and the calculated filter requires 4 multiplications since the zero valued taps can be ignored so it is a question of computational performance versus signal performance. 
\\
\\
Both of them can be efficiently calculated using AVX registers as mentioned in Sec.~\ref{sec:simd}. If the calculated filter from Eq.~\ref{eq:hilbertcalc} is used then the analytic signal for two turns can be calculated simultaneously since it only requires 4 multiplications and one AVX register can hold 8 single precision floating points. Listing~\ref{lst:hilbert} shows how this can be accomplished using Intel intrinsics as described in Sec.~\ref{sec:simd}.
\begin{minipage}{\linewidth}
\begin{lstlisting}[language=C++,caption={Hilbert transform using Intel Intrinsics},label=lst:hilbert]
__m256 constants = _mm256_set_ps(-0.2122, -0.6366, 0.6366,
0.2122, -0.2122, -0.6366, 0.6366, 0.2122);

__m256 turn_data = _mm256_set_ps(turn[0], turn[2], turn[4],
turn[6], turn[1], turn[3], turn[5], turn[7]);

__m256 res = _mm256_mul_ps(constants, turn_data);
__m128 lower = _mm256_extractf128_ps(res, 1);
__m128 upper = _mm256_extractf128_ps(res, 0);
//Sum
lower = _mm_hadd_ps(lower, lower);
__m128 XI0 = _mm_hadd_ps(lower, lower);
upper = _mm_hadd_ps(upper, upper);
__m128 XI1 = _mm_hadd_ps(upper, upper);
\end{lstlisting}
\vspace{2mm}
\end{minipage}
Where XI0 and XI1 corresponds to $x_i[0]$ and $x_i[1]$ in Eq.~\ref{eq:hilbert}. Because $h[n]$ satisfies the symmetry condition  $h[n]=-h[M-n]$ for $0\leq n\leq M$ the phase is exactly $90^{\circ}$ plus a linear component corresponding to a delay of $n_d=3$ turns. So to correct for this, the real signal $x_r[n]$ needs to be delayed 3 turns to be in phase with $x_i[n]$.
\begin{lstlisting}[language=C++,label=lst:amplitudeCalc,caption={Instantaneous amplitude calculation using Intel intrinsics }]
//samples
float samples[16]={};
//Result from Hilbert transform
float hilbert[16]={};
__m256 Q = _mm256_load_ps(samples);
__m256 I = _mm256_load_ps(hilbert+3);
__m256 Qpow = _mm256_mul_ps(Q, Q);
__m256 Ipow = _mm256_mul_ps(I, I);
__m256 res = _mm256_add_ps(Qpow, Ipow);
//square contains the instantaneous amplitude
__m256 square = _mm256_sqrt_ps(res);
\end{lstlisting}
\vspace{2mm}
After this step the instantaneous amplitude is calculated as seen in Listing~\ref{lst:amplitudeCalc}, and is ready to be analyzed by any potential instability detection algorithm. 
\end{document}
