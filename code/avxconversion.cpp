#include <smmintrin.h>
#include <pmmintrin.h>
#include <immintrin.h>
#include <iostream>

int main(){
short data[8]={1,2,3,4,5,6,7,8};
//load 8 short int
__m128i a0 = _mm_loadu_si128((const __m128i *)data);
//split into two registers
__m128i b0 = _mm_unpackhi_epi64(a0, a0);
//convert to 32 bit integers
a0 = _mm_cvtepi16_epi32(a0);
b0 = _mm_cvtepi16_epi32(b0);
//convert to 32 bit float
__m128 c0 = _mm_cvtepi32_ps(a0);
__m128 d0 = _mm_cvtepi32_ps(b0);
std::cout<<c0[0]<<" "<<c0[1]<<" "<<c0[2]<<" "<<c0[3]<<" "<<d0[0]<<" "<<d0[1]<<" "<<d0[2]<<" "<<d0[3]<<std::endl;


}
