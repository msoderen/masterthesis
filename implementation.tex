\documentclass[thesis.tex]{subfiles}
\begin{document}

\chapter{Implementation and Architecture}
\label{cha:implementation}
This chapter explains the architecture of the system in Sec.~\ref{sec:architecture} and the implementation of the system in Sec.~\ref{sec:actualimplementation}. 
\section{Architecture of the ADT Instability Detection System}
\label{sec:architecture}
This section explains the overall software architecture of the system and also results from tests that verified that the system could be implemented.
\subsection{Verifying That FESA Can Handle the High Bandwidth Data Streams}
\label{sec:fesaverification}
FESA as described in Sec.~\ref{sec:fesa} is the standard framework at CERN for developing equipment software. If it can be used in this project it would make the development more straightforward since that would require a more complicated deployment of the software together with a more complicated integration to the CERN infrastructure. There is no known project at CERN which uses the framework in this manner so the function must be verified before a design decision is made. One fact that makes the verification simpler is that the data will be transfered between two FESA classes which are running on the same physical machine. It also helps that the data from the ObsBoxBuffer FESA class as described in Sec.~\ref{sec:obsbox} have a time stamp which tells when the buffer was frozen. With this information, it is easy to calculate the time it takes for the data to reach its destination.
\\
\\
The ObsBoxBuffer FESA class and the new ADT transverse instability detection class will run on the same physical machine so the data will never reach the physical network. It will instead be routed through the internal loopback device. The bandwidth of the loopback device on a target machine was measured using iPerf \cite{iperf} for a quick sanity check and the measured result was $\approx$9000~MB/s. The required bandwidth is $11245\cdot3564\cdot2~$B/s$\approx$80~MB/s so any potential limitation should lie in the FESA framework. To test this, a simple FESA class was created with an association relationship to the ObsBoxBuffer class. This means that the test class is aware of this class and can subscribe to properties of that class.
\\
\\
To make it as real as possible the test class used a multi-threaded event producer which handled the subscription. When new data was available it attached the data as a payload to a real-time event and triggered a real-time action that could analyze the data. 
\begin{lstlisting}[language=C++,caption={Multi-threaded event producer},label={lst:eventproducer}]
class MultiThreadedCESProducer
{
public:
MultiThreadedCESProducer(Device* device)
{
  proxyInterface.subscribe(device->BufferName.getAsString(),
  "Acquisition", "");
}
bool produceEvent(fesa::RTEvent& event)
{
  std::string deviceString;
  std::string propertyString;
  std::auto_ptr<PropertyData> data = proxyInterface.waitNotification(
  deviceString, propertyString);
  boost::shared_ptr<RTEventPayload> payload(new OnSubscriptionRTEventPayload(
  deviceString, propertyString, data));
  event.setPayload(payload);
  payload.reset();
  event.setMultiplexingContext(event.getMultiPlexingContext());
  return true;
}

private:
fesa::ProxyInterface proxyInterface;
}
\end{lstlisting}
The function \textit{produceEvent} in Listing~\ref{lst:eventproducer} is called in a loop from the underlying FESA framework. This eventProducer triggers a real-time event which can extract the payload and do the analysis. To measure the time it takes for the data to reach the real-time event from the point the buffer is frozen, the difference between the trigger stamp in the payload and the system time is printed. The buffer is frozen periodically every 4096 turns.
\begin{lstlisting}[language=C++,caption={Real-time event},label={lst:onmultithreaded}]
class OnMultiThreadedCES : public OnMultiThreadedCESBase 
{
public:
OnMultiThreadedCES(){};
void execute(fesa::RTEvent* pEvt)
{
  std::auto_ptr<const ObsBoxBuffer::AcquisitionPropertyData> data =
  OnSubscriptionRTEventPayload::extract<ObsBoxBuffer
  ::AcquisitionPropertyData>(*pEvt);
  std::cout<<fesa::getSystemTime()-data.triggerStamp.get()<<std::endl;
  //This is a 4096 times 3564 matrix containing all the positional data
  fesa::ImmutableArray2D<int16_t> positional_data = data->getData();
}
};
\end{lstlisting}
Both the trigger stamp and the system time have nanosecond resolution and the time was measured 300 times. From these samples, the maximum time was 469~ms and the minimum time was 429~ms with an average of 447~ms. By also measuring the time the producer waits for new data and the time it took for the real-time action to receive the data, Fig.~\ref{fig:transferobsbox} could be created. From Fig.~\ref{fig:transferobsbox} it is visible that the data transfers overlap. The setup from Listing~\ref{lst:onmultithreaded} and Listing~\ref{lst:eventproducer} was left running over 24 hours and during that time the skew between triggering and receiving the data was stable with an average of 447~ms. 
\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{figures/transfer.eps}
\caption{Diagram of data transfer times from ObsBoxBuffer}
\label{fig:transferobsbox}
\end{figure}
\noindent
The data transfer was the only real concern regarding FESA because when the data has been received in the real-time event any potential parallel programming paradigm in the C++ language is available to be used for parsing the data. It will also make it easier to distribute the result from the analysis over CERN since it can be published through a FESA interface.

\subsection{Proposed System Design}
The proposed solution for the ADT transverse instability detection system is to create a FESA class with 4 instances, one per available server, which handles one transverse plane each. It is proposed that the data stream from the Q7 pickup is used since it has the best SNR. The new class will have an association relationship with the already existing ObsBoxBuffer class which allows it to start subscriptions to the properties of that class. The shortest 4096 turns buffers for the Q7 pickup will be configured to periodically freeze every 4096 turns so the new class receives a full-rate data stream. The new class will have a multi-threaded event producer which subscribes to these buffers and creates a real-time event which triggers a real-time action in which the data analysis can be performed. The data analysis in the real-time event must be able to analyze 4096 turns worth of data in 364~ms, preferably less to have a margin, as not to saturate the available hardware. If an instability is detected, a trigger will be sent through the LIST network. The LIST network has a FESA class for interfacing with it and in the first version it is simplest to have an association with that FESA class and send a trigger that way and in the future do it properly using dedicated hardware. A block diagram of the proposed structure can be seen in Fig.~\ref{fig:instabilityoverview}.
\begin{figure}[H]
\center
\includegraphics[width=0.9\textwidth]{figures/designoverview.eps}
\caption{Block diagram of the system design}
\label{fig:instabilityoverview}
\end{figure}
\noindent
\subsection{Potential Limitations in ObsBoxBuffer's Capacity}
The ObsBoxBuffer class is used in multiple applications and is a vital tool for observing the state of the beam. It is used for diagnosing the ADT, long time frame spectrum analysis to analyze how seismic activity affects the beams, post-mortem analysis of the beams and injection drift observations to name a few. It is also used frequently for machine development which means that during some periods of time, a vast amount of data is generated and saved. There is a possibility that the ObsBoxBuffer class cannot handle this load. Stress tests must be performed after implementation to verify that the new ADT instability detection system does not interfere with the normal operations. It could be that the project interferes with the normal operations either through saturating ObsBoxBuffer with multiple requests at the same time or that the instability detection uses too much of the available computational resources. This could be solved by splitting the fibers from the BPMs to a new server dedicated to online analysis. 

\subsection{Proposed Structure for Exploiting the Algorithm Level Parallelism}
\label{sec:propstruc}
From Sec.~\ref{sec:fesaverification} it seems clear that the new ADT transverse instability system can be implemented as a FESA class, but it is still not decided how the data will be analyzed when it is available in the real-time event.
The implementation does not necessarily need to be portable between platforms since the implementation will most likely only run on the ADTObsBoxes in SR4 so all optimizations can be done with these servers in mind. This means that inline assembly and Intel intrinsics are perfectly valid options, the only problem with this is maintainability. The pipeline pattern which was discussed in Sec.~\ref{sec:pipeline} fits the use case of the data very well since it will process a digital signal in multiple stages. This is a very specific application for a specific platform so the use of existing libraries which abstracts away the platform is not really necessary. To be able to handle the throughput required while keeping the latency as low as possible it makes sense to program as close to the hardware as possible and not introduce any abstraction layers that might cause overhead. 
\\
\\
The idea is that the real-time action will serialize the data which is received from the ObsBoxBuffer to turn-by-turn data and push it into a pipeline which contains the appropriate stages to detect instabilities. The stages will be fairly small and the items will be pretty small ($3564\cdot4B\approx14kB$) so how the worker/item relation is set up does not really matter. 
\begin{figure}[H]
\center
\includegraphics[width=0.75\textwidth]{figures/pipeline.eps}
\caption{Block diagram of the pipeline design}
\label{fig:adtoverview}
\end{figure}
\noindent
The first step is done in the FESA real-time action where the data is serialized and converted to single precision floating points. What it does is to take each row in the matrix that is received from the ObsBoxBuffer class, convert it and store it in a structure that can be passed between stages in the pipeline. To speed up loading into SIMD registers it is best if the memory is 32~byte aligned. 
\begin{lstlisting}[language=C++,caption={QueueElement used in pipeline},label={lst:queueelement}]
class QueueElement {
public:
 QueueElement(std::size_t size);
 ~QueueElement();
 void InsertNewdata(float* input_data, std::size_t size);
 void InsertNewdataExtra(float* input_data, std::size_t size);
 void ChangeSize(std::size_t size);
 //used to store normal data
 float* data;
 //number of elements in data;
 std::size_t data_size;
 //used for extra data after computing I and Q
 float* data_extra;
 //number of elements in data_extra
 std::size_t data_extra_size;
 //internal counter of turns we have calculated
 unsigned long long turn;
 std::shared_ptr<unsigned> bunches;
private:
 //real size of data,
 std::size_t data_size_real;
 //real size of data_extra
 std::size_t data_extra_size_real;
protected:
};
\end{lstlisting}
The class that is shown in Listing~\ref{lst:queueelement} is used to pass data through the pipeline. It has two pointer members since it must contain both the real signal and the companion signal after the Hilbert transform, as described in Sec.~\ref{sec:hilbert}. The system was designed so that the set of bunches which are being analyzed can change during runtime through the FESA interface and instances of QueueElement are being reused since it is expensive to allocate and deallocate memory all the time. That is why the class has members to distinguish between real size and data size because if the number of bunches to analyze decreases in size the same memory is used but not all of it. 
\\
\\
To pass data between stages a simple blocking queue was implemented which used condition variables, see Sec.~\ref{sec:racesynch}, to notify the next stage that data is available for consumption. This means that the stages which have no work to do will be sleeping and not waste computer cycles. This does create some overhead. This is a blocking configuration but it could also be implemented using a lock-free alternative. The reason for blocking is the fact that the data arrives in bursts and most of the time some stages in the pipeline will not have anything to do. With lock-free configuration these will wait for data and use CPU cycles while with a condition variables they will be suspended until data is available.
\begin{lstlisting}[language=C++,caption={BlockingQueue used in pipeline},label={lst:blockingqueue}]
template<typename T>
class BlockingQueue{
public:
BlockingQueue() :mtx(), full_(), empty_(), capacity_(MAX_CAPACITY) { }
void Put(const T& task){
  std::unique_lock<std::mutex> lock(mtx);
  while(queue_.size() == capacity_){
    full_.wait(lock );
  }
  assert(queue_.size() < capacity_);
  queue_.push(task);
  empty_.notify_all();
}

T Take(){
  std::unique_lock<std::mutex> lock(mtx);
  while(queue_.empty()){
    empty_.wait(lock );
  }
  assert(!queue_.empty());
  T front(queue_.front());
  queue_.pop();
  full_.notify_all();
  return front;
}
private:
mutable std::mutex mtx;
std::condition_variable full_;
std::condition_variable empty_;
std::queue<T> queue_;
size_t capacity_;
};
\end{lstlisting}
\noindent
To communicate with the pipeline a data structure is shared between the real-time action and all the stages in the pipeline. This allows for changing settings in the pipeline during operation, extract debug data, extract performance data and also extraction of detected instabilities because only the real-time action has access to send triggers through the list. It would have been possible to do this in the pipeline but to make the pipeline easy to debug and do performance testing it was best to not have any dependencies on CERN software. This meant the pipeline could be tested on a local computer without access to the technical network. 
\\
\\
Since all stages in the pipeline and the real-time action are executing concurrently the data in the structure must be protected against race conditions as mentioned in Sec.~\ref{sec:racesynch}. When possible, it is best to use non-blocking synchronization such as atomic operations and only if it is really necessary then use mutexes. The structure can be seen in Listings~\ref{lst:status} and two different techniques for synchronization are used. Integers and booleans supports atomic operations which solves the synchronization problem but for single precision floating points, a mutex was used.
\begin{lstlisting}[language=C++,caption={Status structure which is used to communicate with the pipeline},label={lst:status}]
class Status {
public:
Status(){
  last_time_data_was_zero=
  (std::atomic_ullong*)malloc(3564*sizeof(std::atomic_ullong));
  for(std::size_t i=0;i<3564;i++){
    *(last_time_data_was_zero+i)=0;
  }
  transverseActivityMonitor=(float*)malloc(3564*sizeof(float));
};
~Status(){

};
//used to keep track of the last time a datastream consisted of zeros
std::atomic_ullong* last_time_data_was_zero;
//used to send all instantaneous amplitudes to the device
//so they can be displayed in the CCC
float* transverseActivityMonitor;
//scales the transverseActivityMonitor 
float scaling;
//used to configure new Hilbert coefficients
float* hilbert;
//Signal that there are new coefficients available
std::atomic_bool new_hilbert;
//Used to send detected instabilities to the real-time action
BlockingQueue<Unstable*>* win1queue;
//for how long not to send triggers after injection
std::atomic_ullong prevent_injection;
//protect float arrays
std::mutex* mtx;
//used to analyze each stage in the pipeline for a single bunch
std::atomic_int* pipeline_analyzer;
//tells the pipeline which bunch to analyze 
std::atomic_uint bunch_to_analyze;
//the current turn number
std::atomic_llong turns;
};

\end{lstlisting}
The stages are simple functions which are target functions for Posix threads as described in Sec.~\ref{cha:industry}. All stage functions take an input queue, an output queue, and the status structure as input parameters. After creation and initialization, each stage waits for data to arrive in the input queue, do the data transformation, and put the item in the output queue. 

\subsection{Exploiting Data-Level Parallelism}
Most computations performed in the pipeline stages will be vector operations performed on long vectors consisting of up to 3564 elements.
To maximize throughput and fully utilize the available hardware the SIMD registers in the Intel Xeon processors should be utilized. This could be done either by relying on the compiler to optimize all the loops over the elements using auto-vectorization or by manually programming the operations. Since the standard compiler at CERN is GCC 4.4.7 which is an old version released 2012, one year after AVX was introduced, the choice fell on using Intel Intrinsics. Since this is a specific application for a specific platform where throughput is critical this was the best way to guarantee optimized usage of the hardware. The intrinsics were chosen using \cite{intelAVX} which is a tool for chosing intrinsics. From the intel guide it is possible to filter out what operation is desirable. There are multiple combinations which can achieve the same results and they were chosen to mimic the normal C++ code as much as possible. No multiply-accumulate instructions were used but it could be used to further improve performance.


\subsection{Proposed Algorithm to Detect Instabilities}
\label{sec:proposed}

The proposed algorithm is the moving average as described in Sec.~\ref{sec:movingaverage} since it is simple and it can use multiple stages in a pipeline to have different window lengths for detecting a wide variety of instabilities. 
\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{figures/movingaverage.eps}
\caption{Block diagram of instability detection part of the pipeline}
\label{fig:movingaveragepipeline}
\end{figure}
\noindent
If the configuration in Fig.~\ref{fig:movingaveragepipeline} is used then the first stage would receive the instantaneous amplitude at a frequency of 11245~Hz and every 256 samples it will compare the average of the latest 256 samples with the average of the 256 samples before that. If the new average is larger than the previous average times a threshold value, a trigger is sent through the LIST network, see Sec.~\ref{sec:list}. This is done per bunch in the LHC so it is known which bunch is unstable and which window detected it. The first stage of length 256 passes the average to the next stage of length 1024 which does the same analysis but on every fourth sample. The second moving average stage passes the average to the third stage which does the same comparison on every fourth sample. It is also possible to calculate the rise time from the averages which can give information about the underlying cause of the instability as discussed in Sec.~\ref{sec:quadrupole}.
\section{Implementation of the ADT Instability Detection System}
\label{sec:actualimplementation}
This section explains the implementation of each stage in the computational pipeline.
\subsection{Retrieve the Data and Triggering a Real-Time Event}
\label{sec:retrieve}
The data is retrieved just as discussed in Sec.~\ref{sec:fesaverification}. There is a simpler way to set this up using the FESA framework but this makes the subscription static and it was preferable to be able to change the subscription during operation. The ObsBoxBuffer allows subscription to a subset of bunches/turns and through the interface of the ADT instability detection system it should be possible to modify which bunches in the machine are being analyzed. When a user modifies the selected filter, the multi-threaded event producer will change the subscription so only the bunches defined by the filter are received. This can be used to lower the stress on the servers.

\subsection{Serializing the Data and Converting It from Signed Integer to Single-Precision Floating-Point in the Real-Time Action}
The procedure for serializing the data is quite simple. For every row in the matrix received from ObsBoxBuffer as described in Sec.~\ref{sec:retrieve}
a new QueueElement is created, see Listing~\ref{lst:queueelement}. By using the technique in Sec.~\ref{sec:conversion} each row is converted to single precision floating points and passed to the next stage. Here, some optimizations could be made; there is a BlockingQueue as described in Listing~\ref{lst:blockingqueue} which passes back old QueueElements from the last stage which has already been used to reduce the time needed for allocating and deallocating memory each time. The complete implementation of this can be seen in Appendix~\ref{app:action}. The reason for the float conversion and why all data the manipulation is done using floating-point numbers is is the complexity of the analysis. This could be implemented using only fixed-point numbers and it would probably be faster but it would require a much greater care to compensate for quantization noise.

\subsection{Injection Oscillation Triggering Prevention}
The first stage in the pipeline which runs on a separate thread is the stage that prevents sending triggers on injection oscillation. When a bunch is injected into the LHC, the data stream will first consist of zeros and then contain heavy oscillations until it is dampened by the ADT. This oscillation will result in triggers from the instability detection stages unless precautions are taken. In the Status structure, there is an array of 64~bit unsigned integers which describes the latest turn where each bunch had a data stream of zeros. This stage checks if five consecutive turns have zeros in the data stream. If that is the case the probability of it being an empty bunch is pretty big. When this is the case the value for that bunch in the array in Status is updated to the turn number of the latest turn. This value is later used in the instability detection stages to verify that it is not an injection oscillation. The complete implementation can be seen in Appendix~\ref{sec:oscilappendix}.

\subsection{Notch Filter}
The second stage in the pipeline is the notch filter which centers the transverse position around zero. It is a FIR filter with coefficients:
  \begin{align}
    h &= \begin{bmatrix}
           1 &
           -1
         \end{bmatrix}
  \end{align}
This is implemented using a circular buffer of length two. When the stage is created it fills the buffer with two elements and after that it enters a while loop which runs forever. In the while loop, it applies the notch filter on the two elements in the circular buffer and stores the result in the oldest element. That element is then pushed to the next stage in the pipeline, a new element is pushed to the circular buffer and the procedure repeats. The whole implementation can be viewed in Appendix~\ref{sec:notchappendix}.

\subsection{The Hilbert Transform Stage}
After the notch, the data is passed through the blocking queue to the stage which calculates the analytic signal for each bunch. This is implemented with a circular buffer of length 7 since we are using a 7 tap FIR filter to compute the transform.
  \begin{align}
    h &= \begin{bmatrix}
           -0.0906 &
           -0.0198 &
           -0.5941 &
           0 &
           0.5941&
            0.0198&
           0.0906
         \end{bmatrix}
  \end{align}
When the stage is created it fills the circular buffer and enters a forever loop. Every time a new item is available it calculates the analytic signal for each bunch and puts the result in the oldest item which is passed to the next stage. It also copies the data from the newest item to the item which is three turns “older” to adjust the phase as described in Sec.~\ref{sec:hilbert}. It was discussed in Sec.~\ref{sec:hilbertopt} which coefficients should be used, either the ones which required 4 multiplications or the ones which required 6 multiplications. The first implementation used 4 multiplications which meant that 2 bunches could be calculated simultaneously. But it was discovered and will be discussed in Sec.~\ref{sec:performance} that this stage was not limiting the pipeline so in the end 6 multiplications were used for a better signal. The whole implementation can be seen in Appendix~\ref{app:hilbert}.

\subsection{The Amplitude Stage}
The amplitude stage is very simple. Every time an item is available, this stage takes it and computes the instantaneous amplitude of the real signal and the analytic signal as described in Eq.~\ref{eq:amplitude} in Sec.~\ref{sec:hilbert}. The implementation can be seen in Appendix~\ref{app:amplitude}.

\subsection{The Maximum Stage}
This is an afterthought because the transverse activity that was being displayed in the CCC showed the average transverse activity from the last 4096 turns. It meant that higher frequency variations in the transverse activity could be smeared out and not be visible in the CCC display. To overcome this limitation, this stage which keeps track of the largest observed instantaneous amplitude from the latest 4096 turns was added. It has an array of floats which contains the maximum value. Every time an item is available it compares the values in the array with the new values and updates the array. Every 4096 turns this array is copied to the Status structure and then reset to zero. The whole implementation can be seen in Appendix~\ref{app:maximum}.

\subsection{Moving Average / Instability Detection Stage}
The reason why this project was created was to detect amplitude growth in the bunch-by-bunch positional data streams from the ADTObsBox and this is done in this stage. The process used is described in Sec.~\ref{sec:movingaverage} and the length of the windows is passed as a parameter when the stage is created. Multiple of these stages can be used one after another to cover amplitude growth with different rise times. It contains a circular buffer which is of the same length as the window, and two arrays which contain the current summation for each bunch and an old summation for each bunch. When a new item is available the values are added to the current summation array and the values from the oldest item in the circular buffer is removed from the summation and then the new item is pushed to the circular buffer. 
\\
\\
A counter keeps tracks of the number of items acquired and when it is equal to the window length, the values in the array containing the old summation are multiplied with the threshold and compared with the new summation. If this is bigger for any bunch, a structure is passed to the real-time event through a blocking queue. After this, the new summation is copied to the array containing the old summation and to the oldest element in the circular buffer, after being divided by the window length, which is passed to the next stage and the counter is reset. The whole implementation can be seen in Appendix~\ref{app:instability}.

\subsection{Transverse Activity Monitor Stage}
The last stage is a simple stage which copies the data after the last average window to the Status structure so it can be copied to a FESA field and displayed in the CCC. The whole implementation can be seen in Appendix~\ref{app:activitymonitor}.

\end{document}
