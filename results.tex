\documentclass[thesis.tex]{subfiles}
\begin{document}
\chapter{Results and Discussion}
\label{cha:resultsdiscussion}
This chapter explains how the system was tested and presents the results from these tests together with a discussion regarding the results.
\section{Performance Evaluation}
This section describes how the pipeline was optimized together with the evaluation of the computational performance of the data pipeline after the optimization.
\subsection{Optimizing the Pipeline}
\label{sec:optimizing}
To analyze the performance of each stage separately, appropriate drivers were created which generated data that could be sent to the stage without any other part of the pipeline interfering. A stub was also created which just cleared the output queue when data was available. To analyze potential bottlenecks in each stage multiple different techniques were used. First Callgrind was used, see Sec.~\ref{sec:tools}, to see where most time was spent in each stage. Some optimizations could be done here, such as replacing some vectors with arrays.
\\
\\
After profiling each stage, the throughput of each stage was measured. The driver which generated data waited a certain period before sending new data and this time was decreased every 32768 “turns”. If the size of the input queue was larger than 4096 then that stage was saturated. The normal operations for making a thread sleep such as usleep and std::this\_thread::sleep\_for could not be used because the time to make a thread sleep and wake up was greater than the shortest time required to saturate some stages. To overcome this, an unoptimized loop was used where the number of iterations decreased over time and the time between sending new data to the stage was measured with the high\_resolution\_clock. The implementation of the driver can be seen in Listing~\ref{lst:stagedriver}. The setup for testing the complete pipeline was similar but a complete matrix was generated instead. 
\begin{minipage}{\linewidth}
\begin{lstlisting}[language=C++,caption={Driver for testing each stage},label={lst:stagedriver}]
signed long timeToWait = 100000;
std::chrono::high_resolution_clock::time_point lastInsert =
std::chrono::high_resolution_clock::now();
unsigned long counter = 0;
while (true) {
  for (std::size_t i = 0; i < timeToWait; i++) {
    asm("");
  }
  QueueElement* temp = new QueueElement(3564);
  temp->data_size = 3564;
  temp->bunches = bunches_pointer_shared;
  st12->Put(temp);
  if (st12->Size() > 4096) {
    std::cout << "failed at " << static_cast<unsigned long long>
    (std::chrono::duration_cast< std::chrono::nanoseconds > 
    (std::chrono::high_resolution_clock::now() - lastInsert)
    .count()) << std::endl;
  }
  lastInsert = std::chrono::high_resolution_clock::now();
  counter++;
  if (counter == 32768) {
    counter = 0;
    if (timeToWait > 1000) {
      timeToWait -= 1000;
    } else {
      timeToWait -= 10;
    }
    std::cout << "time reduced to " << timeToWait << std::endl;
  }
}
\end{lstlisting}
\end{minipage}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/throughput.png}
\caption{Throughput of each stage in the pipeline and the complete pipeline}
\label{fig:throughput}
\end{center}
\end{figure}
Figure~\ref{fig:throughput} shows the throughput for the every stage in the pipeline ( a higher bar is better) and also for the complete pipeline. It is clearly visible that the stage which prevents injection oscillation triggering is the limiting factor. The average measured throughput of that stage was 38275 turns per second which is approximately 240~\% better than required (11245 which is the revolution frequency of the LHC). The average throughput of the complete pipeline was 20820 turns per second which is around 85~\% better than required which is a reasonable safety margin. The reason why this is the limiting stage is that every time a new item is available this stage has to go through the item and look up which bunch is in which place since the set of bunches being analyzed can change. If it is an empty bunch it also has to access the memory in the Status structure and update it. If greater performance is required then this is where optimization is needed. The implementation could be improved if the conversion and serialization stage pads the data so data items with 3564 bunches are always passed through the pipeline.
\\
\\
To visualize the activity in each stage Intel VTune was used, see Sec.~\ref{sec:tools}. In Fig.~\ref{fig:pipelinenotsat} it is visible that each stage has time to recover before new data is available while in Fig.~\ref{fig:pipelinesat} it is visible that the injection prevention is becoming saturated and cannot handle any higher throughput. This data was captured during the throughput tests mentioned earlier. 
\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{figures/pipelinestart.png}
\caption{Activity in each stage when the pipeline is not saturated}
\label{fig:pipelinenotsat}
\end{center}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{figures/pipelineend.png}
\caption[Activity in each stage when the pipeline is saturated]{Activity in each stage when the pipeline is saturated. The Injection prevevntion stage could not handle any higher throughput}
\label{fig:pipelinesat}
\end{center}
\end{figure}

\subsection{Performance Comparison Between Different Compilers}
\label{sec:performance}
To see how auto-vectorization performance has increased over the last years the same tests as described in Sec.~\ref{sec:optimizing} were compiled with three different compilers and executed. The compilers were GCC 4.4.7 since it is the standard compiler at CERN, GCC 5.4.0, and ICC 17.0.0. ICC 17.0.0 was chosen because it is the latest Intel compiler and GCC 5.4.0 because it was released around the same time as ICC 17.
\\
\\
To see how the compilers handled auto-vectorization all stages had two implementations, one with manual optimizations using Intel intrinsics and one normal implementation using standard operations. Both of them were compiled with maximum optimization enabled.
\begin{lstlisting}[language=bash]
-std=c++0x -O3 -msse4.1 -mavx -march=native 
\end{lstlisting}
The results from all tests can be seen in Fig.~\ref{fig:compilercomparison}. There is a clear difference between manual vectorization and auto-vectorization when using the older GCC version, every stage is faster. All loops were perfectly vectorizable and the memory was already aligned for AVX registers but in most cases, it still used scalar operations when examining the generated assembly code. The results for the old and the new GCC compiler when using intrinsics are pretty close but when normal operations are used the newer one has improved or remained the same in all stages except the maximum stage. The only thing that is done at that stage is a comparison and an assignment. When examining the assembly code it is clear that both GCC compilers use scalar operations as can be seen in Listings~\ref{lst:gcc447asm}~and~\ref{lst:gcc540asm}. The major difference is that the older GCC iterates over three scalar instructions so it does one comparison each iteration and then branches. The newer version, however, does 8 comparisons in each iteration and with each comparison, there is a possible branching. The reason for the decrease in throughput could be bad branch prediction in the pipeline. The ICC compiler, however, generates perfect vector code, see Listings.~\ref{lst:ICCasm}. It even uses 8 registers so the instructions can be independent in the instruction pipeline.
\\
\\
The ICC was the only compiler that generated more efficient executables from unoptimized code than from the manually optimized code. Most stages could handle a higher throughput when the normal code was used and this reflected in a higher throughput through the whole pipeline. Figure~\ref{fig:throughputComplete} shows the throughput through the complete pipeline when compiled with the different compilers. Unoptimized code compiled with ICC could handle approximately 42000 turns per second while the manually optimized code could only handle 32000 turns per second.

\begin{figure}[H]
\begin{center}
\includegraphics[width=\textwidth]{figures/compilercomparisonAll.png}
\caption{Comparison of throughput using GCC and ICC}
\label{fig:compilercomparison}
\end{center}
\end{figure}
\begin{minipage}{\linewidth}
\begin{lstlisting}[language=C++,caption={ICC Maximum assembly},label={lst:ICCasm}]
  406a50: c5 fc 10 04 8e        vmovups (%rsi,%rcx,4),%ymm0
  406a55: c4 c1 7c 5d 0c 88     vminps (%r8,%rcx,4),%ymm0,%ymm1
  406a5b: c4 c1 7c 11 0c 88     vmovups %ymm1,(%r8,%rcx,4)
  406a61: c5 fc 10 54 8e 20     vmovups 0x20(%rsi,%rcx,4),%ymm2
  406a67: c4 c1 6c 5d 5c 88 20  vminps 0x20(%r8,%rcx,4),%ymm2,%ymm3
  406a6e: c4 c1 7c 11 5c 88 20  vmovups %ymm3,0x20(%r8,%rcx,4)
  406a75: c5 fc 10 64 8e 40     vmovups 0x40(%rsi,%rcx,4),%ymm4
  406a7b: c4 c1 5c 5d 6c 88 40  vminps 0x40(%r8,%rcx,4),%ymm4,%ymm5
  406a82: c4 c1 7c 11 6c 88 40  vmovups %ymm5,0x40(%r8,%rcx,4)
  406a89: c5 fc 10 74 8e 60     vmovups 0x60(%rsi,%rcx,4),%ymm6
  406a8f: c4 c1 4c 5d 7c 88 60  vminps 0x60(%r8,%rcx,4),%ymm6,%ymm7
  406a96: c4 c1 7c 11 7c 88 60  vmovups %ymm7,0x60(%r8,%rcx,4)
\end{lstlisting}
\end{minipage}

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=C++,caption={GCC 4.4.7 Maximum stage assembly},label={lst:gcc447asm}]
  407260: c5 fa 10 04 86        vmovss (%rsi,%rax,4),%xmm0
  407265: c5 fa 10 0c 82        vmovss (%rdx,%rax,4),%xmm1
  40726a: c5 f8 2e c8           vucomiss %xmm0,%xmm1
  40726e: 76 09                 jbe    407279 <removed address>
\end{lstlisting}
\end{minipage}

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=C++,caption={GCC 5.4.0 Maximum stage assembly},label={lst:gcc540asm}]
  40882f: c5 fa 10 41 18        vmovss 0x18(%rcx),%xmm0
  408834: c5 fa 10 4a 18        vmovss 0x18(%rdx),%xmm1
  408839: c5 f8 2e c8           vucomiss %xmm0,%xmm1
  40883d: 76 05                 jbe    408844 <removed address>
\end{lstlisting}
\end{minipage}
  
  \begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/compilercomparisonThroughput.png}
\caption[Comparison between different compilers]{Comparison of throughput through the complete pipeline using GCC and ICC, with and without manual optimization} 
\label{fig:throughputComplete}
\end{center}
\end{figure}
\noindent
These results are not very general and only apply to this specific problem but it seems that compilers are getting better at auto-vectorization and in the future, data-level optimizations might be limiting the compiler to fully optimize the executable. It is, of course, possible to use the ICC compiler in a FESA project by compiling the code into a static library and then link it to the project.

\section{Functional Results}
This section describes how well the system could detect beam instabilities.
\subsection{Testing the Algorithm in an Offline Environment}
\label{sec:offline}
A test environment was created to visualize how the ADT instability detection system behaved when fed with different signals. It had a simple graphical interface which was created using Qt5. A simple signal generator was implemented which generated a sinusoid with variable amplitude and oscillation frequency:
  \begin{equation} 
  f[t]=A \cdot sin(2\pi f t)
\end{equation}
  \begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/CopraTestSuite.png}
\caption{Test environment}
\label{fig:copratestsuite}
\end{center}
\end{figure}
\noindent
The test environment also allowed for loading stored HDF files which had been saved during machine development runs. These are normally 32768 turns long because they were captured using the 32k buffer in the ObsBoxBuffer FESA class and contained data from both stable and unstable beams. An example of an unstable beam can be seen in Fig.~\ref{fig:unstable_beam} and a stable beam can be seen in Fig.~\ref{fig:stable_beam}. This test environment was in the end mostly used to see where the system detected instabilities after it had been tuned using the automated tuning process described in Sec.~\ref{sec:tuning}.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/unstable_beam.png}
\caption{Unstable beam}
\label{fig:unstable_beam}
\end{center}
\end{figure}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/stablebeam2.png}
\caption{Stable beam with orbit drift}
\label{fig:stable_beam}
\end{center}
\end{figure}
\subsection{Automated Tuning of the Moving Average Threshold}
\label{sec:tuning}
The first step towards automatic tuning was to find relevant data. There was plenty of data stored but there was no metadata which described what each file contained. To solve this plots for all files with all bunches were generated to manually filter out interesting files. Patterns like the one in Fig.~\ref{fig:unstable_beam} were coveted. When enough files had been selected, each bunch in each file was plotted and from that separate bunches were selected. The data for the bunches which contained instabilities were extracted into separate HDF files with added metadata which described where the instability began and ended.
\\
\\
The only available tuning is to configure the threshold for each window. In the current configuration there are three windows of length 256, 1024, and 4096 turns which were chosen because of the normal rise-times of instabilities observed in the LHC. The automatic tuning was done using a Python script which tested evenly spaced thresholds for each window and then recorded which instabilities were detected and any false triggers. From this data, the configurations of the different thresholds for each window which detected most instabilities and as few false triggers as possible were found. This was done according to Algorithm~\ref{alg:autotuning}
\begin{algorithm}[H]
hits=[]\;
misses=[]\;
 \For{Every window}{
  \For{Threshold=0;Threshold<2;Threshold+=0.1}{
 \For{Every File}{
 Run the data in the file through the pipeline and record where it trigger\;
 \If{Trigger was in an instability}{
 add it to hits\;
 }
 \Else{
 add it to misses\;
 }
 }
 }
 }
 Find the configuration of different thresholds for each window which achieves the maximum amount of hits at possible and as few misses as possible\;
 \caption{How to automatically tune the threshold}
\label{alg:autotuning}
\end{algorithm}
\noindent
The rating score for different configurations was calculated with +1 for any instability detection and -5 for every false trigger. The scores were achieved from testing, the value for the false trigger was decreased until a low amount of false triggers were achieved. The data which was used to test the algorithm contained as much normal data with a stable beam as data with instabilities. The result of the tuning was a threshold of 2 for the window with length 256 samples and 2.22 for the other two windows. This means that the first window of length 256 will detect instabilities with a rise time shorter than 22~ms, the second window with length 1024 will detect rise times shorter than 100~ms and the last will detect rise times shorter than 400~ms. With this configuration, the ADT instability detection system detected 95~\% of all instabilities with 5~\% percent of all triggers being false when tested on the extracted data. 

\subsection{Setting up the System }
Most of the development took place during the extended year-end stop of LHC 2016/2017 which meant that testing could only be done with stored or simulated data as described in Sec.~\ref{sec:offline}. After the startup in May 2017, it was possible to test the ADT instability detection system with real data. The system was deployed and ready to be tested before the first beam in the machine and all the data that was generated was logged in the LHC logging system. The data could be accessed through the TIMBER interface, see Sec.~\ref{sec:tools}.
\\
\\
The ADT instability detection system publishes data about the detected instabilities every time it receives new data from the ObsBoxBuffer class, which is approximately every 364~ms. The data which is published consistes of several different data fields:
\begin{itemize}
\item A boolean vector of length 3564 which describes which bunches are unstable.
\item A single boolean which is the result of all the values from previous vector OR:ed.
\item An integer vector of length 3564 which describes which window detected an instability. The windows are represented as 1,2 and 4. If the first bunch is deemed unstable by the second and third window then the value in first position in the vector is 6.
\item A floating point vector of length 3564 which contains the average instantaneous oscillation amplitude for each bunch from the last 4096 turns.
\item A floating point vector of length 3564 which contains the maximum instantaneous oscillation amplitude for each bunch from the last 4096 turns.
\item A floating point number which contains the average of all non zero average oscillation amplitudes.
\item A floating point number which contains the maximum of all maximum oscillation amplitudes. 
\end{itemize}
It was also desirable to introduce a proper service to save the actual bunch-by-bunch positional data from the ObsBoxBuffer, not only for this project but for several other projects as well. To support this a new FESA class called \textit{ADTBufferSaver} was developed. It was a simple class that could be configured to subscribe to up to four buffers. When any of these buffers was updated, the data would be saved as an HDF file. It is now used for several different purposes. It saves all injection buffers on every injection in the LHC for long term drift observation. It saves the new instability buffers when instabilities are detected and also post-mortem buffers which are frozen when the machine dumps the beam. When any of this freeze, the ADTBufferSaver would save the data onto the network file system. An overview of how the system works together can be seen in Fig.~\ref{fig:buffersaver}. The new instability buffers are 65536 turns long and there are four of them (one for each transverse plane) which receives data from the Q7 pickup since the instability detection uses that pickup.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/buffersaver.eps}
\caption{How the ADTBufferSaver fits in the system}
\label{fig:buffersaver}
\end{center}
\end{figure}
The following list describes how the different systems work together, as visualized in Fig.~\ref{fig:buffersaver}.
\begin{enumerate}
\item The ObsBox class collects data which is stored in a large circular buffer, a periodic trigger saves a subset of the data into a 4096 turns long buffer
\item When the 4096 turns long buffer is updated, the instability detection analyzes the data and detects an potential instability
\item It sends a hardware trigger to the LIST network
\item The new 65536 turns buffer for instability is frozen by the LIST trigger
\item The ADTBufferSaver class receives the data from the 65536 turns long instability buffer
\item The data is saved on the network file system for later analysis
\end{enumerate}
\subsection{Setting the Thresholds}
In the first setup for testing the system, the thresholds which were calculated from the automatic tuning were used but it turned out that they were too high since there were no triggers being generated. Not even during injection with injection prevention disabled. The conclusion from this was that the stored data did not represent the normal state of the LHC very well and a new procedure for setting the thresholds was thought up:
\begin{algorithm}[H]
 \For{Every window}{
  Disable all other windows by setting a very high threshold (>10)\;
  Set a high enough threshold (>4) on current window so there are no triggers\;
  \While{No triggers}{
  lower threshold by 0.1\;
  }
  Increase threshold for current window by 0.2 to have a margin;
 }
 \caption{How to set the threshold for the trigger}
\label{alg:threshold}
\end{algorithm}

\noindent
This was done for all four transverse planes during flat top with stable beams. They were configured using the FESA navigator and the triggers from the system could be observed using the FESA navigator as well. The margin was chosen arbitrary to be 0.2 because a margin of 0.1 still generated some sporadic triggers, the result from this was (VB1= vertical beam 1,HB2= Horizontal beam 2):
\begin{table}[H]
\begin{center}
\begin{tabular}{ l| c | c | c |r|}
  &VB1 & VB2  & HB1 &HB2\\ \hline
  256  & 1.8 & 1.8 & 2.0 & 3.0 \\ \hline
  1024 & 1.5 & 1.5 & 1.8 & 2.5 \\ \hline
  4096 & 1.4 & 1.4 & 1.5 & 2.0 \\ \hline
\end{tabular}
\end{center}
\caption[]{Threshold after applying Algorithm~\ref{alg:threshold}}
\end{table}
\noindent
The reason for the high thresholds in the horizontal plane in beam two turned out to be caused by a glitch randomly appering in the data stream which caused the bunch position to be registered as 0 for one turn, as can be seen in Fig.~\ref{fig:streamglitch}. This caused the instantaneous amplitude to momentarily increase rapidly due to the notch filter, which triggered the instability detection. The cause has not been investigated yet at the time of writing so the instability detection for that plane is more or less disabled. It could possibly still be useful for triggers from the longest window where the short amplitude increase is less noticeable.
\\
\\
The last step was to configure for how long the injection oscillation triggering prevention would be enabled. This was done by using the thresholds that were produced experimentally and observing the generated triggers during injection. The number of turns which it was disabled was incremented by steps of 1024 until triggers were no longer generated during injection. The result from this was that no triggers were sent for bunches before 12288 turns had passed after they were injected. After this configuration, the system was completely autonomous and could collect data from instabilities.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/streamglitch.png}
\caption{Glitch in the data stream for HB2}
\label{fig:streamglitch}
\end{center}
\end{figure}
\subsection{Tools To help Analyze the Collected Data}
\label{sec:analyzetools}
Every file that was collected with the help of the ADT instability detection system was saved on CERN's network file system (NFS) where the ADT had 6~TB of space to store data. This space was used for post-mortem, injection, and instability data. All files had the same naming convention:
\begin{lstlisting}[language=bash,caption={Filename convention}]
06177_Inst_B2H_Q7_20170908_13h18m42s.h5
\end{lstlisting}
Where the first part describes during which LHC fill it was generated, the second describes what generated the file (Inst=Instability, Inj=Injection, Pos=Post-mortem), the third which transverse plane, the fourth which pickup and the last two which time it was frozen. They were organized in the following file structure:
\dirtree{%
.1 nfs.
.2 cs-ccr-bqhtnfs.
.3 fillnr.
.4 postmortem\_data.
.4 instability\_data.
.4 injection\_data.
.3 plots.
.4 fillnr.
.3 scripts.
}
\noindent
A Python script was created which used the time in the filename to extract which bunches were unstable that time from the LHC Logging System by using PyTimber. After that, the relevant bunches could be plotted to see if they showed signs of amplitude growth. Another Python script ran that script for every file in a specific fill folder. This meant that after a fill in the LHC, the script could be started and when done, the plots could be inspected manually.
\subsection{Results From Online Testing}
The complete system with the data acquisition using ADTBufferSaver was fully implemented the 5th of September. By the 26th of September 2017, 253~GB of data divided over 76 LHC fills had been collected by buffers freezing because of triggers sent by the ADT instability detection system. By using the Python scripts mentioned in Sec.~\ref{sec:analyzetools} plots could be generated after every fill to analyze the performance of the instability detection. From the 580~TB which was analyzed during this period, only 253~GB was saved and, when manually analyzing the data only ,about 10\% contained interesting activities. There was still a lot of triggers generated during injection which generates a lot of unnecessary data. Injection oscillation in the injected bunches was not the only problem; when they were injected, they perturb and change the orbit of bunches in the other beam, which caused a rise in amplitude, see Fig.~\ref{fig:injectionperturb}.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/perturb.png}
\caption{Beam perturbed by injection}
\label{fig:injectionperturb}
\end{center}
\end{figure}
\noindent
To be able to completely ignore triggers during injection a new feature was added which disabled the system when the energy in the LHC was lower than a specified value. The value could, for example, be set to 6.5~TeV to only detect instabilities when the machine was at flat-top (fully accelerated). The thresholds could also be increased to further limit the amount of data generated but it was at a manageable level. 
\\
\\
Examples of activities that were captured can be seen in \Cref{fig:62001,fig:62002,fig:62211,fig:62212,fig:62271,fig:62272}. To analyze the data which was stored required quite a lot of manual work and it would be possible to use this system as a first level data filter and then use more advanced techniques on the filtered data. In the end, the system worked well with only a few bugs that had to be fixed, such as making sure that the queue with instability data was cleared before more data was pushed to the pipeline. If this was not done there was a chance for a potential deadlock.
{
\setlength{\floatsep}{0pt}

\begin{figure}[H]
\centering
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6200_1.png}
  \captionof{figure}{Excitation for coupling measurement during fill 6200 using the ADT as exciter}
  \label{fig:62001}
\end{minipage}%
\hspace{2mm}
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6200_2.png}
  \captionof{figure}{Amplitude growth during fill 6200}
  \label{fig:62002}
\end{minipage}
\end{figure}
\squeezeup
\begin{figure}[H]
\centering
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6221_1.png}
  \captionof{figure}{Short excitation for tune measurement during fill 6221 using the ADT as exciter}
  \label{fig:62211}
\end{minipage}%
\hspace{2mm}
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6221_2.png}
  \captionof{figure}{Low frequency orbit drift during fill 6221}
  \label{fig:62212}
\end{minipage}
\end{figure}
\squeezeup
\begin{figure}[H]
\centering
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6266_1.png}
  \captionof{figure}{The amplitude of bunch 731 during fill 6266 with slow rise time}
  \label{fig:62271}
\end{minipage}%
\hspace{2mm}
\begin{minipage}[t]{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6227_2.png}
  \captionof{figure}{Rapid amplitude growth during fill 6227}
  \label{fig:62272}
\end{minipage}
\end{figure}
}

\subsection{Usage of the real-time Transverse Activity Monitor in the CCC}
The display in the CCC has become an important operational tool to observe the real-time bunch-by-bunch transverse activity. It is used as an early warning for bunch instability or detection of unwanted transverse excitation (i.e. by injection cleaning or injection kicker edge displacement). It allows the operators of the LHC in the CCC to monitor the peak activity over time, slow activity (average over 4096 turns), fast activity (maximum over 4096 turns) and the evolution of activity over time. The calculated activity is calibrated in micrometers, so the impression of how severe the activity is immediately visible. Everythingis displayed per bunch so it is always possible to determine which bunch is becoming unstable. Examples of logbooks entries from the LHC operators can be seen in  \Cref{fig:log3,fig:log4,fig:log5}. 
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/log3.png}
\caption[Entry in the LHC operator logbook]{Entry in the LHC operator logbook after an instability occurred because the Landau damping was disabled}
\label{fig:log3}
\end{center}
\end{figure}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/log4.png}
\caption[Instability shown in the ADT transverse activity monitor]{Instability shown in the ADT transverse activity monitor. The red line is the maximum oscillation amplitude achieved and the black line is the current amplitude which is a average from the last 4096 turns}
\label{fig:log4}
\end{center}
\end{figure}
\noindent
Easy availability of the real-time transverse activity immediately triggered many questions about the performance of various subsystems and helped to explain some long observed features during the beam injection cycle.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{figures/log5.png}
\caption{Multiple bunches were unstable and this was visible thanks to the fixed display}
\label{fig:log5}
\end{center}
\end{figure}
\subsection{Real Life Example on How the System Helped Scientists at CERN}
\label{sec:reallife}
On the morning of the 3rd October 2017, the logbook showed an entry from the day before (02/10/2017 20:18) which contained a capture from the ADT Transverse Activity Monitor, the capture can be seen in Fig.~\ref{fig:log6}. This showed a clear increase in transverse activity during fill 6266, when squeezing the beam. By using the TIMBER application it was clear that the system had triggered multiple times during this period and there were multiple stored HDF files thanks to ADTBufferSaver. By using the Python script which extracts data about the unstable bunches from TIMBER and by using some Bash scripting (see Listing~\ref{lst:plotscript} ), plots for the relevant period could be easily created. The result from this can be seen in \Cref{fig:62662,fig:62663,fig:62664,fig:62665}. This information was sent to relevant people in the ABP (\textit{Accelerator and Beam Physics}) group at CERN so they could analyze the data stored by the ADTBufferSaver and figure out the cause of the instability. These findings were presented the following day (4th October) at the morning LHC operations meeting. The cause was quickly found and fixed for the next injection. 
\begin{lstlisting}[language=bash,label={lst:plotscript},caption={Plotting all unstable bunches from all planes during a specific time}]
for file in 6266/instability_data/06266_Inst_B*_19*; do 
  python scripts/plotinstabilityfile.py -notch -f "$file" -max 10 ;
done 
\end{lstlisting}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/log6.png}
\caption[Screenshot of the ADT transverse Activity Monitor]{Screenshot of the ADT transverse Activity Monitor in a entry in the LHC OP logbook the 2nd October 2017 20:18 which shows increased bunch-by-bunch transverse activity between 19:10 and 19:40 the 2nd October}
\label{fig:log6}
\end{center}
\end{figure}

\begin{figure}[H]
\centering
\begin{minipage}{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6266_2.png}
  \captionof{figure}[Some unstable bunches]{Bunches 1239, 1240, 1314, 1811, 2502 and 2922 were deemed unstable during fill 6266 by the new system at 19:22:25}
  \label{fig:62662}
\end{minipage}%
\hspace{2mm}
\begin{minipage}{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6266_3.png}
  \captionof{figure}[Some unstable bunches]{Bunches 917, 2032, 3204 and 3239 were deemed unstable by the new system at 19:24:05}
  \label{fig:62663}
\end{minipage}
\end{figure}
\begin{figure}[H]
\centering
\begin{minipage}{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6266_4.png}
  \captionof{figure}[Some unstable bunches]{Bunches 522, 731, 735, 1679 and 2761 were deemed unstable by the new system at 19:28:10}
  \label{fig:62664}
\end{minipage}%
\hspace{2mm}
\begin{minipage}{.48\textwidth}
  \centering
  \includegraphics[width=\textwidth]{6266_5.png}
  \captionof{figure}{Bunches 699, 732, 2949 and 3356 were deemed unstable by the system at 19:29:21}
  \label{fig:62665}
\end{minipage}
\end{figure}
\noindent
There was a lot of interest regarding the instability during the squeeze in the LHC morning meeting and part of one slide can be seen in Fig.~\ref{fig:meeting}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/meeting.png}
\caption{Part of the slide from the LHC morning meeting 4th October}
\label{fig:meeting}
\end{center}
\end{figure}

\subsection{An Example of How the System Detects Instabilities}
This section describes how the data is processed in the pipeline. The data in this example was captured during the instability that occurred during the squeeze of fill 6266 as already described in Sec.~\ref{sec:reallife}.
\\
\\
Figure~\ref{fig:rawdata} shows the raw data captured by the ADTBufferSaver FESA class after it received a trigger from the LIST network. The trigger was generated by the ADT instability detection system. There is, of course, a delay from the point that the 4096 turns buffer has been sent to the system to when an instability has been detected and the longer 65536 turn buffer has been frozen. This delay is however compensated by an offset in the instability buffer of 32768 turns which means that the ADTBufferSaver gets data which is 32768 turns old. 
\\
\\
The data is first sent through a notch filter and the result can be seen in Fig.~\ref{fig:rawnotch}. The notch filter is there for closed orbit suppression, which means that it centers the data around 0. After the notch filter, the analytic signal is calculated and from this, the instantaneous oscillation amplitude can be calculated as described in Sec.~\ref{sec:hilbert} and shown in Fig.~\ref{fig:rawamplitude}. The last step in the pipeline is to calculate the moving averages and compare consecutive values to detect any rapid amplitude increase. The moving averages together with the generated triggers can be seen in Fig.~\ref{fig:rawaverage}.
\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{figures/raw.png}
\caption{Raw positional data for bunch 735 during the squeeze of fill 6266 in the LHC}
\label{fig:rawdata}
\end{figure}
\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{figures/notch.png}
\caption{Data after notch filter}
\label{fig:rawnotch}
\end{figure}
\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{figures/amplitude.png}
\caption{Instantaneous oscillation amplitude calculated using the Hilbert transform}
\label{fig:rawamplitude}
\end{figure}
\begin{figure}[H]
\center
\includegraphics[width=\textwidth]{figures/averagemoving.png}
\caption[Moving average of the instantaneous amplitude]{Moving average of the instantaneous amplitude and the generated triggers, the window length is 1024 and the trigger threshold was 1.4}
\label{fig:rawaverage}
\end{figure}

\section{Method Discussion}
The architecture of the system was defined fairly early in the process for simplicity and practical reasons. Only the FESA framework was considered for implementation and only the pipeline pattern was considered for extracting algorithm level parallelism. It would have been interesting to test different implementations using different parallel programming paradigms, not only the pipeline pattern, but the time for this project was limited. It would also have been interesting to test different instability detection algorithms, especially exponential curve fitting. The method itself was appropriate for the scope of this thesis, and future evaluation of online instability detection in high energy particle accelerators are left for future studies. 

\end{document}