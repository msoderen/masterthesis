\documentclass[thesis.tex]{subfiles}
\begin{document}

\chapter{Related Work}
\label{cha:related}
There are multiple systems implemented for detecting transverse oscillation amplitude growth. For example the BBQ, Head-Tail monitor, MIM and soon the ADT transverse instability detection system. All of them have their advantages and disadvantages. The biggest challenge is sampling the signal from the pickup because the length of a bunch in the LHC is 1.5~ns which means that the sampling frequency must be in the order of 6-12~GHz \cite{Steinhagen:1637764}. At these speeds, the resolution is limited even with state-of-the-art technology. The different systems tackle this problem in different ways.

\section{The LHC Head-Tail Monitor}
The Head-Tail monitor was originally designed for machine parameter extraction but it can also be used for analyzing the beam stability \cite{Cocq:370134}\cite{instabilityMonitoring}. Is uses a stripline pickup just as the ADT and the signal are passed through a hybrid filter which generates a sum and difference signal just as in the ADT. This signal is however not passed to a BPM but to a very fast digitizer which samples the signal at 10 GSPS ($10\cdot10^{9}$ samples per second) with 8bit resolution. This high sampling rate means that it can see intra-bunch motion, meaning that it can see the shape of the bunch compared with the ADT that just sees the centre of charge for the whole bunch. The problem with this high sampling rate is the amount of data that is created. For now, it can only store 11 turns (1 ms) of data and this data takes 10 seconds to download from the digitizer. This means that a full-rate bunch-by-bunch extraction is not possible, but more modern digitizer are being tested that will allow for longer acquisitions. Figure~\ref{fig:headtailoverview} shows an overview of the system and Fig.~\ref{fig:headtailmode4} shows an instability that was detected during 2015.
\begin{figure}[H]
\includegraphics[width=\textwidth]{figures/head-tail2.png}
\caption{Overview of the head-tail monitor system (courtesy of CERN)}
\label{fig:headtailoverview}
\end{figure}
\begin{figure}[H]
\center
\includegraphics[width=0.8\textwidth]{figures/head-tail1.png}
\caption{A mode 4 instability captured by the head-tail monitor (courtesy of CERN)}
\label{fig:headtailmode4}
\end{figure}

\section{The LHC Base-Band Tune System (BBQ)}
The BBQ is a diode peak detector which converts a high-frequency signal from a pickup to a low-frequency signal that can be sampled with high-resolution audio ADCs \cite{Gasior:1476069}. An overview of the system can be seen in Fig.~\ref{fig:bbq}. It is used for nonintrusive beam parameter extraction, meaning that it can extract beam parameters without exciting the beam.
\begin{figure}[H]
\center
\includegraphics[width=0.8\textwidth]{figures/bbq.png}
\caption{Overview of the LHC BBQ system (courtesy of CERN)}
\label{fig:bbq}
\end{figure}
\noindent
This system allows for extremely high turn-by-turn resolution up to 30nm compared to 1~$\mu$m in the ADT and 50~$\mu$m with a normal button pickup. It also has excellent signal-to-noise ratio but this comes at a price. The BBQ signal which is sampled is an average of all transverse oscillations of all bunches in the LHC. So it cannot detect transverse bunch-by-bunch instabilities. But it can detect that there is an ongoing instability on some bunch if that instability is large enough so it becomes the dominant component of the signal. There is ongoing research regarding instability detection using the BBQ which has shown great promise but all instability detection is FPGA based \cite{instabilityMonitoring}.

\section{The Multiband-Instability-Monitor (MIM)}
The MIM is a prototype being developed and tested at CERN, it is currently being tested in both SPS and LHC.
The MIM tackles the required high resolution in another way than the BBQ. Just as with the ADT and the BBQ, it uses a stripline pickup but afterward the signal is split into several frequency bands using an RF filter bank and with narrower frequency bands the signal can be sampled with much higher resolution ADCs and also in parallel. However, with the current strip-line pickups which only have an effective bandwidth of up to 6GHz, the number of effective channels are 16 spaced by 400MHz. Better pick-ups are being developed to support up to 12GHz which would allow up to 32 channels. This system is, however, mainly designed to measure intra-bunch motion and it is still in its development phase \cite{Steinhagen:1637764}.

\section{Algorithms for Instability Detection}
Detecting an outlier in a time series is not straightforward and there has been a lot of statistical research done in, for example, trend analysis to predict stock prices. The problem here, however, is fairly simple because during normal operation, the bunch transverse oscillation amplitude will be stable and the only thing that needs to be detected is an exponential growth increase. See Fig.~\ref{fig:unstable1}, it shows real data for one bunch in the LHC which started to become unstable after injection. 
\begin{figure}[H]
\center
\includegraphics[width=0.8\textwidth]{figures/unstable1Edited.png}
\caption[Transverse position of an unstable bunch in the LHC]{Transverse position of an unstable bunch in the LHC. The bunch was injected and the injection oscillation was quickly damped by the ADT but after 20000 turns in the LHC, ($\approx$2~s) the amplitude of the transverse oscillation increased rapidly which lead to an unstable bunch.  }
\label{fig:unstable1}
\end{figure}


\subsection{Moving-Average}
\label{sec:movingaverage}
A simple and robust way of detecting an exponential amplitude growth in a time series is to calculate the moving average over a window with length $W$ and compare that with the next window.
\begin{figure}[H]
\center
\includegraphics[width=0.8\textwidth]{figures/unstableavg.png}
\caption{Moving average over windows with $W$=1024}
\label{fig:average}
\end{figure}
\noindent
Calculating the average for the $m$:th window is straight forward:
  \begin{equation}
MA_m=\dfrac{1}{W}\sum_{i=mW}^{(m+1) W}A_i
  \end{equation}
Where $A$ is from Eq.~\ref{eq:amplitude}.
And during normal operations it is assumed that:
  \begin{equation}
MA_m \approx MA_{m+1}
  \end{equation}
And during an exponential amplitude growth is it assumed that:
  \begin{equation}
MA_m \ll MA_{m+1}
  \end{equation}
To adjust the threshold for a trigger:
  \begin{equation}
MA_m \cdot T < MA_{m+1}
  \end{equation}
If $T=1.5$ is used, this requires the next window average to be 50\% higher than the previous window to send a trigger. To be able to detect instabilities with a variety of rise times, multiple moving averages with different window length can be combined. To reduce the number of calculations needed, the different averages can be stages in a pipeline where the average from the first window is passed to the second stage. 

\subsection{Three-Averages Algorithm}
\label{sub:three-averages}
The three-average algorithm has been tested at CERN both in the MIM and the BBQ. It computes the average of the standard deviation about the mean of the signal over three different time windows. The different window lengths are arranged such that $W_{short}<W_{med}<W_{long}$. From these windows $\sigma_{short}$, $\sigma_{med}$ and $\sigma_{long}$ are calculated and it is assumed that during normal beam  $\sigma_{short}\approx\sigma_{med}\approx\sigma_{long}$ holds. During an exponential amplitude growth, the average of a longer window will grow slower than the one of the short windows. The following inequalities should hold during an instability:
  \begin{equation}
  \begin{aligned}
\sigma_{short}-\alpha \sigma_{med}>0 \\
\sigma_{med}-\beta \sigma_{long}>0
  \end{aligned}
  \label{eq:threeaverages}
  \end{equation}
Where $\alpha,\beta>0$ are coefficients chosen to set the trigger threshold. For a higher level of confidence the numbers of turns in the machine where the inequalities in Eq.~\ref{eq:threeaverages} are true are counted and when the counter reaches a threshold it sends a trigger. This was implemented on FPGAs in both the MIM and the BBQ.
During tests, it was discovered that this algorithm does not perform well when the rise time is slow since the averages follow the slow increase and the difference is never big enough to generate a trigger \cite{instabilityMonitoring}.

\subsection{Increase-Subsequence Algorithm}
The Increase-Subsequence algorithm has also been tested in the BBQ and the MIM. It keeps track of the latest $W$ samples and every time a new sample is acquired it checks if the new sample is larger than the maximum value from the subset. If that is the case, a counter is incremented or if the oldest value in the subset is the largest, the counter is decremented. When the counter reaches a certain threshold a trigger is generated.
\\
\\
During tests, it was discovered that it handles long rise times better compared to the Three-Averages algorithm. However, when there are many bunches in the LHC the amplitude change is less prominent and the Three-Averages performs better since it is more sensitive \cite{instabilityMonitoring}.

\subsection{Exponential Curve Fitting Using the Least Square Method}
After low-pass filtering the signal to remove high-frequency noise, the signal can be approximated to fit the functional form:
   \begin{equation}
A[n]=e^a e^{bn}
  \end{equation}
Where $A[n]$ is the amplitude from Eq.~\ref{eq:amplitude} and $n$ is the turn-number.
The least-square fitting can be found using (with $A_n$ instead of $A[n]$ and over $W$ samples):
   \begin{equation}
a=\dfrac
{\sum^{W}_{n=1} (n^2 A_n) \sum^W_{n=1}(A_n \ ln(A_n))- \sum^W_{n=1}(n A_n)\sum^W_{n=1}(n A_n \ ln(A_n)) }
{\sum^W_{n=1}A_n \sum^W_{n=1}(n^2 A_n)-(\sum^W_{n=1}n A_n)^2}
\end{equation}
   \begin{equation}
b=\dfrac
{\sum^W_{n=n}A_n \sum^W_{n=1}(n A_n \ ln(A_n)) - \sum^W_{n=1}(n A_n)\sum^W_{n=1}(A_n\ ln(A_n))}
{\sum^W_{n=1}A_n\sum^W_{n=1}(n^2 A_n)-(\sum^W_{n=1 }n A_n)^2}
  \end{equation}
Where $a$ is proportional to the linear growth in the amplitude and $b$ is proportional to the exponential growth. During normal operation it is assumed that $b\approx 0$ and during an instability with exponential amplitude growth $b > 0$. This is interesting since the rise time of the instability is automatically acquired \cite{Strutz}.
\begin{lstlisting}[language=C++,caption={[Exponential Curve Fitting]Exponential Curve Fitting Using the Least Square Method implemented using Intel intrinsics}]
float sum(const __m256& x1){
 __m256 temp= _mm256_hadd_ps(x1,x1);
 temp=_mm256_hadd_ps(temp,temp);
 return temp[0]+temp[4];
}
__m256 x;
__m256 y;
float Y=0.0, XY=0.0, X2Y=0.0, YLNY=0.0, XYLNY=0.0;

Y+=sum(y);
__m256 XYv=_mm256_mul_ps(x,y);
XY+=sum(XYv);
X2Y+=sum(_mm256_mul_ps(XYv,x));
__m256 LNYv= _mm256_clog_ps(y);
XYLNY+=sum(_mm256_mul_ps(XYv,LNYv));
YLNY+=sum(_mm256_mul_ps(y,LNYv));
float temp=(Y*X2Y-pow(XY,2.0));
float a=(X2Y*YLNY-XY*XYLNY)/temp;
float b=(Y*XYLNY-XY*YLNY)/temp;
\end{lstlisting}
In this example, the number of samples was only 8 but this can easily be extended to any number of samples. It is important to be able to detect instabilities of different rise times and the range of instabilities that can be detected depends on the number of samples used for the curve fitting. Fig.~\ref{fig:exponential} shows an example of how this can be implemented. The instantaneous amplitude is calculated and passed to one block which does a curve fitting on 128 samples and then decimates the signal by a factor 4. The signal is then passed through a chain of identical blocks where the last block receives a signal which has been downsampled by a factor of 1024. If this was applied to the bunch-by-bunch transverse positional data in the ObsBox which has a sampling frequency of 11245~Hz, then the last block would do a curve fitting on $\approx 11.65$s of downsampled data.
\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{figures/exponential.eps}
\caption{Instability detection pipeline using exponential curve fitting}
\label{fig:exponential}
\end{figure}
\noindent
This can be done without downsampling but that would require the server to keep $\approx$ 1GB of data in memory and every time a new sample is received a new curve-fitting on 131072 samples would have to be done for every bunch which is quite computationally intensive. This is interesting since we are looking for exponential amplitude growth and this would give the rise-time of the instability directly, it could also be more error proof the the current algorithm. No test were done with this algorithm because of time-constraints.


\end{document}