\documentclass[thesis.tex]{subfiles}
\begin{document}

\chapter{Theory}
\label{cha:theory}

\subsection{Transverse Feedback System}
The LHC transverse feedback system(ADT) fill several needs. It damps transverse injection errors, prevents transverse coupled bunch instabilities and can be used for exciting the beam during beam measurements. It consists of four independent systems, one for each beam and plane. Each system consists of two modules and each module have two kickers meaning that the LHC has 16 kickers in total. Each kicker unit has one power amplifier with two tetrodes. All systems use the same pickups called Q7,Q8,Q9 and Q10 which are of type stripline. There are the only a few stripline pickups in the LHC and the majority of pickups are of type button BPM's with lower resolution. The stripline pickups consists of a metal electrode which forms a transmission line along the direction in which the beam travels. When a charged particle travels along the line a current will be induced and the closer the particle is the higher current will be induced(Faraday's law of induction). In the pickup there are four electrodes, two for each plane, and by measuring the difference in the induced current the particles position in the beam pipe can be measured. In reality there are $10^{11}$ particles passing the pickup so the center of mass of the bunch is measured. 
\\
\\
The pickup is connected to a hybrid filter which generates I and Q signals which are passed to a strip-line comb filter which generates 400.8MHz wavelets which are then processed by a Beam Position Module which calculates the transverse position for each bunch. This is then sent over a 1 Gbps fiber-optic link to a mDSPU which does the closed orbit rejection, pick-up vector sum, one-turn delay, phase equalization of the non-linear response of the amplifier and control the bandwidth of the feedback loop. From this the mDSPU generates a analog signal which controls the power amplifiers for the electrostatic kickers. The mDSPU also handles excitement of the beam. Through the FEC many kinds of excitation configurations can be set up. This is used for measuring the tune in the machine and for doing coupling measurements.
\\
\\
A interface for setting up excitations models were created during this thesis which was named ADTACDipole. The application was implemented in FESA, see section~\ref{sec:fesa} and it abstracts away part of the low-level RF system while using an excitation model for excitation predictions to prevent possible beam dumps or bunch blow-ups. It is has many uses such as coupling measurements or tune extractions. It can also be used to simulate bunch instabilities.
\\
\\
This is the only place in the LHC where full-rate bunch-by-bunch positional data is available. But the feedback system is limited by how much data it can buffer. The current configuration can buffer 256 turns and a second limitation is the bandwidth of the VME backplane which only has a bandwidth of 100Mbps so the data can not be streamed from the mDSPU over the VME. To overcome this the fiber-optic link between the beam position monitor and the mDSPU is split so it gets sent to the mDSPU and to a powerful server called a ObsBox which is capable of buffering 6 million turn at the moment.

\section{The ObsBox System}
The ObsBox system was designed to overcome the limitations of the VME bus and allow for an increase in the amount of data available for analysis without any prior filtering or decimation. This system allows for minutes of data buffering instead of milliseconds in the ADT. Given 4 channels per server, to have data for the same plane in one server, which transmits at 0.6 Gbps, and the amount of data which will be stored the server needs hold a considerable amount of memory. Four 6 million turns buffers requires approximately 115Gb of memory so the server needs at least 132 GB of memory. It also requires at least 5 PCI-express slots, four for SPEC cards for the fiber-optic links and one for a timing card to receive timing events. For future development it was also preferable if it had one extra PCIe x16 slot for a GPU. There have already been research done in the RF group regarding bunch-by-bunch tune extraction using GPU's \cite{Dubouchet:1545785}. The choice fell on SuperMicro 6028U-TR4+ which has 5 PCIe slots x8 slots and one x16 slot, all in a 2U rack mounted format. It also supports two Intel Xeon E5-2600 v4/v3 processors and up to 1.5Tb of RAM.
\\
\\
In point 4 there are six SuperMicro 6028U-TR4+, four for the transverse position and two for the longitudinal position, each with dual Intel Xeon E5-2620 and 132Gb of RAM. The four transverse servers are called ADTObsBoxes and each of these receives positional bunch-by-bunch data for one plane from four pickups through four SPEC's. Each SPEC receive data from one pickup where the sampling frequency is the same as the revolution frequency of the LHC(11.2 KHz). These servers run Scientific Linux which is being developed by Fermi National Accelerator Laboratory and is based on Red Hat Enterprise Linux but have been patched with a real-time kernel. The real-time patch guarantees that the latency between an interrupt and the process being called is usually below 10 microseconds. Figure~\ref{fig:system1} shows an overview of the system.
\begin{figure}[H]
\label{fig:system1}
\includegraphics[width=14cm]{figures/system.eps}
\caption{System overview}
\end{figure}
The SPEC cards is a bridge between the fiber-optic link and the RAM on the host computer, it has DMA for fast data transfer. It was developed at CERN by the BE-CO and BE-RF groups. It is a simple 4-lane PCIe carrier for a FPGA Mezzanine card. The driver for the SPEC is based on the ZIO framework which defines the input/output data flow and the user-space interface to access the data. The framework makes it easy to set up buffers, triggers and Linux DMA configurations. The data can be accessed through normal mmap. 
There are multiple requirements for the user-space application which utilizes the driver, such as :
\begin{itemize}
\item The data needs to be available in a user-space application where users can request data at any time
\item The data needs to be frozen in the user-space from an external trigger 
\item The driver can only store a small amount of data in kernel-space before it gets overwritten
\item The application needs to make the data available to user over some standard protocol for easy acquirement
\end{itemize}
To fulfil all these requirements while keeping the software as simple as possible the FESA framework was used. 
The Front-End software Architecture is the standard middle-ware framework that is currently being used at CERN for controlling equipment. The ADTObsBoxes which buffers the bunch-by-bunch positional data has a FESA interface which can be used to access the data.
\\
\\
In the first launch of the ADTObsBox there was a FESA class called ObsBox which allowed download of the full 6 minutes long buffer. This was however very inefficient since it takes long time to download and normally only a fraction of the buffer contains interesting data. To solve this another FESA class called ObsBoxBuffer which had a composition relationship with ObsBox was created which allowed for shorter acquisitions ranging from 4096 turns up to 131072 turns. These buffers can be accessed through a property in ObsBoxBuffer called Acquisition and a subset from this buffer can be acquired from a property called AcquisitionSubset where filters which defines which turns and bunches can be applied.This data can be accessed in many ways:
\begin{itemize}
\item Through the FESA navigator which is a simple generic Java-client application for testing FESA classes
\item Through JAPC which you can integrate in any Java application
\item Through PyJAPC which is a Python interface to JAPC
\item Through CMW which is the control middle ware at CERN
\item From any FESA class that has a association relationship with ObsBoxBuffer
\end{itemize}
The data in the Acquisition/AcquisitionSubset property includes multiple data fields:
\begin{itemize}
\item data: a turns $\times$ bunches matrix of short int which contains the bunch-by-bunch transverse position
\item triggerStamp: a long int which contains the time the buffer was frozen
\item turnCounters: an array of long integers which contains all turn numbers for the acquisition
\item tagBits: an array of long integers which contains tags for all turns
\end{itemize}
There are many use cases for the available data. It can be used for offline analysis where users download the data to their local computer or to remote storage. This can be for machine development, long time injection oscillation observation or drift observations. It can also be used for semi-offline analysis which is normally when a user is sitting in the CCC and pulls data at full-rate or on timing events. Typical semi-offline analysis is activity after injection, scrubbing, tune shift observation, coupling correction and collimator impedance measuring. The ObsBox can also be used for online analysis which is when the calculation is executed on the server. Typical online analysis is beam parameter extraction, damper performance evaluation, transverse activity monitoring and online instability detection.
\\
\\
To use the ADTObsBox for online instability detection a solution would be to create a new FESA class which runs on the same machine on which the data is stored to stress the network. The class would subscribe to the ObsBoxBuffers Acquisition property and the data would be passed to the new class over the internal loop-back.
The instability detection FESA class will setup a Custom event source which subscribes to the Acquisition property and when it is notified it generates a real-time event with the data as a payload. The event triggers a real-time action which trigger a real-time action. In the real-time action anything can be done with the data and output can be published for users and triggers can be sent over the LIST.
\\
\\
The ADTObsBox is an important tool for the LHC operation. It is the only tool which has access to sub-micron bunch-by-bunch transverse positional data at full-rate. There are other similar tools but all of them have their own limitations. There is the LHC head-tail monitor which is used to measure chromaticity, this also uses a stripline pickup and the analogue signal is feed to a state-of-the-art oscilloscope but even then it has limited memory and can only buffer a couple of hundreds of turns. There is the LHC BBQ which is used for tune extraction, this also uses a stripline pickup but the analogue signal is feed through a diode peak-detector and the output is an average transverse motion of all bunches in the LHC. However the positional resolution is greater then in the ADT transverse feedback system. There is also many standard BPMs around LHC but their resolution is not sub-micron. For a single one turn nominal bunch measurement their resolution is 50 $\mu$m rms. There is also the MIM(Multiband-Instability-Monitor) which is being developed
\\
\\
During this thesis another FESA class called ADTSpectrum was developed which calculated the FFT for each bunch's transverse positional data. The 131072 turns long buffers were implemented for this purpose to be able to distinguish low frequency oscillations. This was part of a bigger project to discover if there are causality between seismic activity and oscillations in the LHC. Using the FFTW library a 131072 samples long FFT was calculated for each bunch and oscillation frequency component amplitude was averaged between all bunches and then publish through the FESA interface.

\section{FESA}
\label{sec:fesa}
The Front-End Software Architecture is a project launched 2003 at CERN by the controls group. It is a complete environment for equipment specialist to design, develop, deploy and test equipment software at CERN. This tools helps to standardize, speed-up and simplify the task of developing control software. The FESA infrastructure contains many interconnected components such as:
\begin{itemize}
\item Object-oriented Real-Time Framework: Defines the architecture of the software and lets the programmer focus on the functionality of the control software.
\item Graphical Tools: Graphical applications for generating design, deployment and instantiation of the new FESA class.
\item FESA Design Schema: The complete meta-model of the FESA class. This forces the designer to construct the new class out of several predefined objects:
\begin{itemize}
\item A public interface for controlling the class
\item A device-model which is an abstraction of the underlying hardware
\item A set of server actions which are triggered from the public interface
\item A set of real-time actions which are triggered by logical events
\item A set of logical events which can be triggered by many kinds of sources
\end{itemize}
\item Code generation: From the design, deployment and instantiation XML documents efficient C++ code is generated
\item Test environment: From the design XML document an JAVA GUI is automatically generated for interfacing with the new class during testing, see figure~\ref{fig:fesaNav}
\end{itemize}
\begin{figure}[H]
\label{fig:fesa}
\includegraphics[width=14cm]{figures/fesa.jpg}
\caption{FESA workflow}
\end{figure}
The usage of FESA across all CERN accelerators have lead to a standardized high-level language which is used for developing portable, maintainable and efficient control equipment software. If the developer have used configuration fields in a good manor then deploying new instances of a FESA class on new FECs can be done directly in the CCDB and it can be operational in a short timespan.
\\
\\
The FESA framework is an ideal platform for developing the new LHC instability detection system since it is well known and integrated in the CERN computer structure. It has full support for real-time computing in the Scientific Linux distribution which is used in all FECs at CERN. One limitation for now is that it is only supports GCC 4.4.7 which is a somewhat old version, it has limited C++11 standard support.  
\begin{figure}[H]
\label{fig:fesaNav}
\includegraphics[width=14cm]{figures/fesa.png}
\caption{FESA navigator}
\end{figure}

\section{LHC Instability Trigger Network}
The LIST allows for bi-directional trigger distribution between equipment in the LHC that is capable of detecting instabilities and devices containing relevant information to analyze the cause of the instability. If any device in the LHC ring detects an instability it can send out a trigger with a payload and any device connected to the network can take precaution. Such as freezing their buffer and sending it for long time storage for later analysis.  The LIST network is based on White rabbit technology \cite{white-rabbit} which is a deterministic, synchronous extension to the Ethernet standard. It allows for connected devices to be synchronized with sub-nanosecond precision. Devices connected to the network includes BBQ, Head-Tail monitor, ADT, MIM(Multi-band Instability Monitor) and many more. The idea is to have online instability detection in the ADTObsBox which can decide which bunches are becoming unstable and also the rise-time of the instability. This data can be sent over the LIST network which would result in relevant devices freezing their buffers which concerns the relevant bunches and send it to storage. This would be a game changer since the amount of data that is generated is vast and if the experts already now where to look it will save a lot of time and also storage space. During 2015 the Head-tail monitor alone generated 1TB of data, most of which is not interesting and filtering was done manually. 

\section{Instability Detection in the LHC}
Instabilities in the LHC are a limiting factor of how many bunches can be injected and stored. There are many  causes for self-amplifying beam instabilities such as beam-beam interaction, electron clouding and wake fields. It is important to measure beam properties before, during and after an instability to understand the source and make corrections. Normal symptoms of instabilities are emittance growth and losses on specific bunches. This can happen at any time during operation and it is normally very unpredictable. The emittance growth can be seen in the BSRT but by that time it is too late to react since there is a long latency in the BSRT operation. There is a big need to detect instabilities as they occur to measure relevant beam parameters for future corrections.
\\
\\
Beam-beam interactions happens in the interaction point which in the LHC there are four of. Both beams affect one another since they generate an electromagnetic field. A simple model of this is the strong-weak beam interaction model where one strong beam is considered unperturbed by the other beam and at the same time it acts as an non-linear focal lens. This means that the weak beam periodically perturbed by the strong which can cause an instability.\cite{Chao:1984ef} 
\\
\\
Electron Clouding occurs when charged particles disturb stray electrons in the particle accelerator which makes the electrons hit the beam pipe. This will result in more electrons being emitted because of secondary emissions. This will generate a electric field which could perturb the accelerated particles in the accelerator.\cite{arduini2003electron}
\\
\\
Wake fields are electromagnetic fields which are created when then charged particles in an particle accelerator interact with the vaccum chamber. The coupling can exists because of irregularities in the beam pipe material or geometric features in the beam pipe and this creates a coupled system of particles and electromagnetics fields that may become unstable. Since we have causality in the accelerator the fields exists after the bunch that is coupled with the machine. These fields affects trailing bunches and can be seen as a dynamic magnetic fields which can cause the trailing bunches to oscillate either with too high amplitude so the feedback system can't dampen it or it can be an harmonic revolution frequency so the feedback system don't see it. \cite{Mounet:1451296}
\\
\\
Beam-beam interactions, electron clouding and wake fields can cause head-tail resonance. During a head-tail instability the bunch oscillates internally with higher modes. For example if the bunch is experiencing a mode 1 head-tail oscillation the head and tail are oscillating in a counter-phase, see figure~\ref{fig:headtail}. Resistive wall effect causing wake field can lead to coupled-bunch instabilities which is when the bunch couple with the beam pipe and generates electromagnetic fields which affects the trailing bunches.
\begin{figure}[H]
\label{fig:headtail}
\includegraphics[width=14cm]{figures/head-tailmode1.png}
\caption{Head-tail oscillation with mode=1}
\end{figure}
\noindent
There are multiple systems implemented for detecting transverse oscillation amplitude growth. For example the BBQ, Head-Tail monitor, MIM and soon the ADT transverse activity monitor. All of them have their advantages and disadvantages.
\subsection{LHC Head-Tail Monitor}
The Head-Tail monitor is used for many things such as validating the LHC impedance model and measure chromaticity. It can also detect intra-bunch motion i.e the spectrum of the bunch transverse oscillation. It achieves this by acquisition of transverse BMP sum/difference signals with a extremely fast digitizer(oscilloscope). Problems with the Head-Tail is:
\begin{itemize}
\item Limited dynamic range i.e it can only see larger instabilities
\item Slow readout at 10-15 seconds
\item Short acquisition length at 11 turns
\item Large data files
\end{itemize}
Some of these problems are being fixed by upgrading to a newer digitizer but there are still limitations on what you can do with the data in the oscilloscope so implementing instability detection algorithms are problematic.
\subsection{The LHC BBQ}
The base-line LHC Tune Measurements System relies on the diode-based base-band-tune (BBQ) technique and is used to measure tune, coupling and chromaticity. It uses a stripline pickup which feeds it signal to a diode-peak detector which sends the signal through a analogue front-end and after that is sampled and passed to an FPGA for spectrum analysis. Instabilities can be seen as a growth in the spectrum at the tune frequency. The problem is that spectrum is an average of all bunches oscillation so an instability in a low intensity bunches can be hidden. Two different instability detection algorithms have been tested in the BBQ, the three-average algorithm and the Increasing Subsequence algorithm\cite{instabilityMonitoring}. Both of them have problem differentiate noise from instabilities.
\subsection{The Multiband-Instability-Monitor(MIM)}
The MIM is a prototype being developed and tested in CERN, it is currently being tested in both SPS and LHC. The system splits the signal into several equally spaced frequency bands that are processed in parallel. This allows for much higher resolution ADC's to be used which can result in nm resolution of particle position. However with the current strip-line pickups which only have an effective bandwidth of up to 6GHz the number of effective channels are 16 spaced by 400MHz. Better pick-ups are being developed to support up to 12GHz which would allow up to 32 channels. This system is however mainly designed to measure intra-bunch motion and it is still in its development phase but it shows great promise. \cite{Steinhagen:1637764}
\section{SIMD instructions in x86-64 processors}
SIMD(Single Instruction Multiple Data) instructions are multi processing elements which can be used to exploit data level parallelism. For example a simple vector eight elements is added element-wise to another vector of same length. Without SIMD this takes eight add operations while if there are two SIMD register available of length 8 this can be done in one operation, see figure~\ref{fig:simd}.
SIMD instructions in x86-64 processors have been around since the first Pentium processors with the MMX registers which were 64 bit wide and allowed for 8, 16, 32 or 64 bit integer operations. This was later extended with SSE(Streaming SIMD Extensions). The first generation SSE introduced 8 128 bit long register which allowed operations on 4 32 bit single precision floats in one cycle. In programs where the same operations were applied to many elements such as digital signal processing or graphics processing the number of operations could be increased with 400\% compared to scalar operations. SSE2 introduced operationsn on 8 bit char, 16 bit short int, 32 bit integers, 63 bit integers ans 64 bit double precision floating numbers. There have seens been futher upgrade in SSE3, SSSE3, SSE4 and SSE4.1 which all introduced new features but the biggest change came with AVX. AVX was introduced in 2008 in the Intel Sandy Bridge platform and it extends the SIMD registers to 256 bit length. This allows for simultanious operations on 
4 64 bit double precision floating numbers or 8 single precision floating numbers which can be executed using up to 32 registers. \cite{firasta2008intel}
\\
\\
The AVX registers can be accessed either through inline assembly, high-level intrisics or by compiler auto-vectorization. There are problems with all of these. Both inline assembly and high-level intrisics are error prone and not very readable compared with normal high-level language such as C or C++. The maintainability of the code decreases since the numbers of programmers that can handle this is sparse. But at the same time this gives full control over the program and you don't have to rely on the compiler to do the work for you. In high performance applications and libraries this might be suitable. This does sacrifice portability since some assembler instructions or intrisics might not be available on on some processors. Auto-vectorization in compilers are great for fast optimization, by just setting the correct  optimization flag for the  compiler it will do as much optimizations it can. However research have shown that compilers can vectorize as little as 30\% of perfectly vectorizable loops in applications\cite{Maleki:2011:EVC:2120965.2121464}. Even though this research is old it is still valid at CERN since the standard compiler in their infrastructure is GCC 4.4.7 and the one that was tested was 4.7.0. 
\begin{figure}[H]
\label{fig:simd}
\center
\includegraphics[width=8cm]{figures/simd.eps}
\caption{Principle of SIMD instructions }
\end{figure}
\section{Cache coherency}

\section{Transverse Oscillation Amplitude Calculation}
Regardless of which instability detection algorithm is used the first step after receiving the positional data from the ObsBoxBuffer is to convert it from short int to float/double. This can be done very efficient using AVX registers and Intel intrinsics in the following manner:
\begin{lstlisting}[language=C++,caption={Fast short to float conversion using Intel Intrisics}]
//load 8 short int
__m128i a0 = _mm_loadu_si128(data);
//split into two registers
__m128i b0 = _mm_unpackhi_epi64(a0, a0);
//convert to 32 bit integers
a0 = _mm_cvtepi16_epi32(a0);
b0 = _mm_cvtepi16_epi32(b0);
//convert to 32 bit float
__m128 c0 = _mm_cvtepi32_ps(a0);
__m128 d0 = _mm_cvtepi32_ps(b0);
\end{lstlisting}
After the float conversion the oscillation amplitude for each bunch must be calculated. The obvious approach would be to do a FFT which would give the entire spectrum. FFT is however a fairly heavy operation and an FFT for a 4096 turn long array would require $4096 \times log(4096) \approx 34070$ multiplications. However the most information from the FFT would be uninteresting since it is already known that the oscillation frequency we are interested in is the tune of the machine i.e the fractional part of the betatron oscillation. It is also known that the signal is a causal, stable and real sequence which means we can apply a Hilbert transform. By doing a Hilbert transform this can be reduced to $4*4096=16384$ multiplication. The Hilbert transform can be implemented as a FIR filter and can be tuned for the interesting frequency. It gives us the analytic signal from which the instantaneous amplitude can be calculated. 

\subsection{Analytic Signal Calculation}
A real signal $x_r[n]$ is a one-dimensional array of real values over time. This signal normally has positive and negative frequency components with a symmetry around the zero-frequency point. This signal can be extended by a companion function $x_i[t]$ so that the resulting signal only has positive frequency components. The relation between $x_r[n]$ and $x_i[t]$ is related through the Hilbert transform \cite{salih:2012} \cite{david:2012}.
\begin{equation} 
\label{eq:hilbert}
 x_c[n]=x_r[n]+jx_i[n]
\end{equation}  
Where $x_c[t]$ also can be expressed as:
\begin{equation} 
 x_c[n]=A[n]e^{j\phi[n]}
\end{equation}
and
\begin{equation} 
 A[n]=\sqrt{x_r^2[n]+x_i^2[n]}
\end{equation}
The Hilbert relationship:
\begin{equation} 
\mathcal{H}[x_r[n]]=x_i[n]
\end{equation}
Which can also be expressed as:
\begin{equation} 
\label{eq:causal}
x_i[n]=\sum_{m=-\infty}^{\infty}h[n-m]x_r[m]
\end{equation}
where:
  \begin{equation}
  \label{eq:hn}
    h[n]
    \begin{cases}
      \dfrac{2}{\pi}\dfrac{sin^2(\pi n/2)}{n}, & \text{if}\ n\neq0 \\
      0, & n=0
    \end{cases}
  \end{equation}
However \ref{eq:causal} is not really helpful since it not absolutely summable and also non-causal. That is when a discrete Hilbert transformer of order M can be created with a Kaiser window approximation which instead of \ref{eq:hn} uses:
  \begin{equation}
  \label{eq:kaiser}
    h[n]
    \begin{cases}
      \dfrac{2}{\pi}\dfrac{sin^2[\pi (n-n_d)/2]}{n-n_d}, & \text{if}\ 0\leq n\leq M \\
      0, & \text{otherwise}
    \end{cases}
  \end{equation}
  where:
  \begin{equation} 
n_d=M/2
\end{equation}
So for example with $M=6$:
  \begin{align}
    h &= \begin{bmatrix}
           -0.2122 &
           0 &
           -0.6366 &
           0 &
           0.6366 &
            0&
           0.2122
         \end{bmatrix}
  \end{align}
Since three values are zero these can be ignored so only four multiplications are needed for a Hilbert transform with a kaiser window of length 6.
\\
\\
This can be implemented very efficient in C/C++ using the AVX registers which allows for 256 bit SIMD instructions. The Hilbert transform for two turns can be calculated the following way using Intel intrinsics:
\begin{lstlisting}[language=C++,caption={Hilbert transform using Intel Intrisics}]
__m256 constants = _mm256_set_ps(-0.2122, -0.6366, 0.6366,
0.2122, -0.2122, -0.6366, 0.6366, 0.2122);

__m256 turn_data = _mm256_set_ps(turn[0], turn[2], turn[4],
turn[6], turn[1], turn[3], turn[5], turn[7]);

__m256 res = _mm256_mul_ps(constants, turn_data);
__m128 lower = _mm256_extractf128_ps(res, 1);
__m128 upper = _mm256_extractf128_ps(res, 0);
//Sum
lower = _mm_hadd_ps(lower, lower);
__m128 XI0 = _mm_hadd_ps(lower, lower);
upper = _mm_hadd_ps(upper, upper);
__m128 XI1 = _mm_hadd_ps(upper, upper);
\end{lstlisting}
Where XI0 and XI1 corresponds to $x_i[0]$ and $x_i[1]$ in equation~\ref{eq:hilbert}. Because $h[n]$ satisfies the symmetry condition  $h[n]=-h[M-n]$ for $0\leq n\leq M$ the phase is exactly $90^{\circ}$ plus a linear component corresponding to a delay of $n_d=3$ turns. So to correct for this the real signal $x_r[n]$ needs to be delayed 3 turns to be in phase with $x_i[n]$.
\begin{lstlisting}[language=C++,caption={instantaneous amplitude calculation using Intel intrisics }]
//samples
float samples[16]={};
//Result from Hilbert transform
float hilbert[16]={};
__m256 Q = _mm256_load_ps(samples);
__m256 I = _mm256_load_ps(hilbert+3);
__m256 Qpow = _mm256_mul_ps(Q, Q);
__m256 Ipow = _mm256_mul_ps(I, I);
__m256 res = _mm256_add_ps(Qpow, Ipow);
//square contains the instantaneous amplitude
__m256 square = _mm256_sqrt_ps(res);
\end{lstlisting}
After this step the instantaneous amplitude is calculated and is ready to be analysed by any potential instability detection algorithm. 

\section{Instability detection}  
  
\subsection{Three-Averages Algorithm}
\label{sub:three-averages}
The LHC base-band tune (BBQ) is a diode peak detector that converts a high-frequency signal from a BPM to a low-frequency signal which can be sampled using normal off the shelf ADC. The signal can be seen as the average amplitude of all bunches in the LHC. The three averages is a instability detection algorithm developed by K. Lasocha at CERN. It was tested on the BBQ during 2015. The algorithm uses three windows of different length $W_{short}$, $ W_{med}$ and $W_{long}$ where $W_{short} < W_{med} <W_{long}$. This calculates the standard deviation about the mean over the three windows. During normal conditions it is expected that $\sigma_{short}\approx\sigma_{med}\approx\sigma_{long}$. During a instability it is expected that $\sigma_{short} -\alpha \sigma_{med}>0$ and $\sigma_{med} -\beta \sigma_{long}>0$ holds where $\alpha$ and $\beta$ are coefficients chosen to reduce the influence of noise. 
\\
\\
During tests it was discovered that this algorithm does not perform well when the rise time is slow since the averages follows the slow increase and the difference is never big enough to generate a trigger.\cite{instabilityMonitoring}

\subsection{Increase-Subsequence Algorithm}
This is another algorithm developed by K. Lasocha at the same time as the Three-Averages Algorithm. It uses a subset $S$ of the $n$ latest samples from the BBQ. Every time it gets a new sample it adds it to the subset and finds the maximum value in the subset. If the maximum value is the newest one it increments a counter by one and if it is the oldest the it decrements the counter by one. When the counter reaches a certain threshold a trigger is generated. 
\\
\\
During the tests it was discovered that this algorithm performs better when it comes to long rise times compared to the Three-averages algorithm. However when there are a long number of bunches in the machine the change in amplitude is less prominent and the Three-Averages performs better since it is more sensitive to amplitude changes. Their conclusion is also that the algorithms suffers from the BBQ signal quality when there are many bunches and the feedback system has a high gain.\cite{instabilityMonitoring}

\subsection{Moving-Average}
A very simple way of detecting an exponential rise in a time series is to calculate an accumulative moving average over a window with length $n$ and compare that average with the accumulative moving average of the next $n$ samples. 
\begin{figure}[H]
\label{fig:average}
\includegraphics[width=\textwidth]{figures/average.png}
\caption{Moving average}
\end{figure}
\noindent
If you have a window with length $n$ then the $m$:th cumulative moving average window is:
  \begin{equation}
CMA_m=\dfrac{1}{n}\sum_{i=m\times n}^{(m+1)\times n}x_i
  \end{equation}
And during normal operations it is assumed that:
  \begin{equation}
CMA_m \approx CMA_{m+1}
  \end{equation}
And during an exponential amplitude growth is it assumed that:
  \begin{equation}
CMA_m \ll CMA_{m+1}
  \end{equation}
To adjust the threshold for a trigger:
  \begin{equation}
CMA_m \times T < CMA_{m+1}
  \end{equation}
Could be used where $T=1.5$ would require the next $n$ samples to have an cumulative average 50\% higher than the previous $n$ samples.
\subsection{Exponential Curve Fitting Using the Least Square Method}
After low-pass filtering the analytic signal to remove high-frequency noise the signal can be approximated to fit the functional form:
   \begin{equation}
y=A\times e^{B\times x}
  \end{equation}
Where $y$ is the amplitude and $x$ is the turn-number. If we set:
 \begin{equation}
B \equiv b
  \end{equation}
   \begin{equation}
A = e^a
  \end{equation}
The least-square fitting can be found using:
   \begin{equation}
a=\dfrac
{\sum^{n}_{i=1} (x_i^2 y_i) \sum^n_{i=1}(y_i \ ln(y_i))- \sum^n_{i=1}(x_i y_i)\sum^n_{i=1}(x_i y_i \ ln(y_i)) }
{\sum^n_{i=1}y_i \sum^n_{i=1}(x_i^2 y_i)-(\sum^n_{i=1}x_i y_i)^2}
\end{equation}
   \begin{equation}
b=\dfrac
{\sum^n_{i=n}y_i \sum^n_{i=1}(x_i y_i \ ln(y_i)) - \sum^n_{i=1}(x_i y_i)\sum^n_{i=1}(y_i\ ln(y_i))}
{\sum^n_{i=1}y_i\sum^n_{i=1}(x^2_i y_i)-(\sum^n_{i=1 }x_i y_i)^2}
  \end{equation}
Where $a$ is quite uninteresting since it only contains information about the orbit feedback. Under normal operation it is assumed that $b\equiv B \approx 0$ and during a instability with exponential amplitude growth
$b\equiv B > 0$. This is interesting since the rise time of the instability is automatically acquired. This can be implemented very efficient using Intel intrinsics. 
\begin{lstlisting}[language=C++,caption={Exponential Curve Fitting Using the Least Square Method implemented using Intel intrisics}]
float sum(const __m256& x1){
	__m256 temp= _mm256_hadd_ps(x1,x1);
	temp=_mm256_hadd_ps(temp,temp);
	return temp[0]+temp[4];
}
__m256 x;
__m256 y;
float Y=0.0, XY=0.0, X2Y=0.0, YLNY=0.0, XYLNY=0.0;

Y+=sum(y);
__m256 XYv=_mm256_mul_ps(x,y);
XY+=sum(XYv);
X2Y+=sum(_mm256_mul_ps(XYv,x));
__m256 LNYv= _mm256_clog_ps(y);
XYLNY+=sum(_mm256_mul_ps(XYv,LNYv));
YLNY+=sum(_mm256_mul_ps(y,LNYv));
float temp=(Y*X2Y-pow(XY,2.0));
float a=(X2Y*YLNY-XY*XYLNY)/temp;
float b=(Y*XYLNY-XY*YLNY)/temp;
\end{lstlisting}
In this example the number of samples were only 8 but this can easily be extended to any number of samples. It is important to be able to detect instabilities of different rise times and the range of instabilities that can detect depends on the number of samples used for the curve fitting. Fig~\ref{fig:exponential} shows an example on how this can be implemented. The analytic signal is calculated and passed to one block which does a curve fitting on 128 samples and them decimates the signal by a factor 4. The signal is then passed through a chain of identical blocks where the last block receives a signal which has been downsampled by a factor of 1024. If this was applied to the bunch-by-bunch transverse positional data in the ObsBox which has a sample frequency of 11245Hz then the last block would do a curve fitting on $\approx 11.65$s of downsampled data.
\begin{figure}[H]
\centering
\label{fig:exponential}
\includegraphics[width=\textwidth]{figures/exponential.eps}
\caption{Instability detection pipeline using exponential curve fitting}
\end{figure}
\noindent
This can be done without downsampling but that would require the server to keep $\approx$ 1GB of data in memory and every time a new sample is received a new curve-fitting on 131072 samples  would have to be done for every bunch which is quite computational intensive. 
\section{Vectorization}
\section{Parallel programming patters}
\section{SkePU}

\end{document}